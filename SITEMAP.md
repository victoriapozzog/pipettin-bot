# Site map

Get your bearigs: learn how to navigate the repo here.

## Repository structure

Each subdirectory groups different kinds of source material. In each one, you'll find a `README.md` explaining its contents, and linking to other parts of the repo.

If you are looking for assembly or usage documentation, find it in the documentation repository [here](https://gitlab.com/pipettin-bot/pipettin-grbl-docs).

## [Docs and Guides](./GUIDES.md)

Links to guides and documentation are available at: [GUIDES.md](./GUIDES.md)

## Top-level contents

Contents of the repository at the top level directory.

Find what you are looking for in the more specific folders.

### CAD files: [`models`](./models)

Source model files for the machine's parts, modeled mostly with [FreeCAD](https://www.freecadweb.org/).

![assembly3_tooclhanger_back.png](./doc/models/modelos_XYZS/images/assembly3_tooclhanger_back.png)

Most models are being migrated to asingle folder, here: [all_models](./models/all_models)

### Software modules: [`code`](./code)

All software and firmware for the pipettin-bot project in one [code](./code) directory.

### Default workspaces and objects: [`defaults`](./defaults)

JSON definition for the "default" GUI workspaces, platforms, protocols, etc.

### Development documentation: [`doc`](./doc)

Extra documentation directory (currently in migration to the doc repo and/or other project subdirectories).

See its [README.md](./doc/README.md).

Content in this directory is referenced in the repo's READMEs when relevant.


## Legacy

Older versions of this repository are available in "archive" branches:

- [master-backup-v1-archive](https://gitlab.com/pipettin-bot/pipettin-bot/-/tree/master-backup-v1-archive)
    - Legacy components: GRBL, pigpiod, protocol2gcode, pure JS protocol designer GUI, old directory layout for models.

