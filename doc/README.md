# Development documentation

Documentation is distributed under the terms of the [CC-BY-SA 4.0 licence](https://gitlab.com/pipettin-bot/pipettin-grbl/-/blob/master/LICENSES.md#documents).

> Disclaimer: there are lots of "to-do"s around here.

This directory holds a "development" documentation salad.  The projects's usage and assembly documentation has been moved to [this repo](https://gitlab.com/pipettin-bot/pipettin-grbl-docs) and published in [gitlab pages](https://pipettin-bot.gitlab.io/pipettin-bot-docs).

The most important sub-directories in this "doc/" folder are:

* [BOM](./BOM) contains a shopping list.
* [tool_callibration](./tool_callibration) contains tool calibration procedures.
* [calibration](./calibration) contains gravimetric calibration results for micropipette tools.
* [electronica](./electronica) contains notes on the electronics of the machine.
* [models](./models) contains old development docs of the project's CAD models.
