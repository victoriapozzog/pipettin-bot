In this calibration I changed the pipetting sequences a bit.

Conclusions:

- Tip priming or "wetting" is now separate from the over-draw/back-pour correction.
    - Honestly tip priming did not seem to make much difference. I've yet to try disabling it to evaluate its effect in the final condition.
- p20 works well with 1x 12 uL draw followed by 5x 2 uL pours.
    - The last pour was tested only once but also did well. End-to-end performance achieved.
    - Overal under-pour with 1.9 uL avg. May be fixed by increasing "scaler_adjust" from 1.08 to perhaps 1.10.
    - Lowering the volume to 9uL/5x1.5uL worked but with higher variability and overa under-pouring (1.4 uL avg).
- p200 not tested, added dummy parameters for:
    - scaler_adjust
    - under_draw_correction

