Calibracion con una secuencia en incrementos constantes: subir hasta el tope y bajar hasta el fondo.

https://docs.google.com/spreadsheets/d/1JNC1xFl7ZwNCNkPnPBLw8HRQ9wp5Lkq6KBYjxUF-f_s/edit#gid=0

Relevant observations:

- The steppers were not operating smoothly.
    - I had to "fix" noise problems on the fly by filtering false limit switch triggers. Even so, some homing sequences looked weird, and paused mid-way without warnings.

Discussion:

- **p200**:
    - The p200 tool seems to **load** less volume than expected:
        - Missing 0.2 uL every 5 uL loaded.
        - Missing 1.5 uL every 20 uL loaded.
    - The p200 tool seems to **dispense** more accurately, for both 5 uL and 20 uL moves.
- **p20**:
    - This pipette is harder to calibrate, because the scale is relatively less accurate for these volumes. Higher errors are expected.
    - The p20 tool seems to **load** relatively well (10% for 2 uL, 13% for 0.5 uL).
    - The p20 tool **dispense** badly (37% error for 0.5 uL) or well enough (10% error for 2 uL).
        - In the 37% error case, the dispensed volume was HALF the loaded volume ¿what happened?
        - I repeated the 0.5 uL test with the same results: 9% error to load, 33% error to dispense. The total mass change on dispensing was again close to half of the loaded mass, even though the tip was actually empty at the end. Exactly the same results… ¿is the weigh wrong?
- Errors are average of absolute value, exluding outliers, see [2022-11-30 Multitool calibration.ods](./2022-11-30 Multitool calibration.ods).
- No consistent evaporation errors are expected, leaving the tube for several minutes in teh scale showed no mass loss.