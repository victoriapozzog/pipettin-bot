# Converting songs to GCODE

Each stepper can produce a note from its current stepping frequency (speed).

1. If you wanted the robot to sing then you would use this to start I guess: [freq_to_gcode.ods](./freq_to_gcode.ods)
2. There are two notes on that:
    - Acceleration is limited, so the whole thing will sound like a guitar slide in a way (which would require some optimization to avoid large changes in axis speed).
    - A new GCODE command (with a new duration) must be issued every time a note changes. This means that a longer, continuous note will span multiple GCODE commands.

The spreadsheet is also useful to convert a set of axis speeds (e.g. of multiple syringe pumps at different rates) into a GCODE command that will move them simultaneously at those speeds.