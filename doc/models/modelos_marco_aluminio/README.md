# Printable couplings for 2020 profiles

Table of contents:

[[_TOC_]]

-—

Modelos paramétricos hechos en FreeCAD para acoplar perfiles de aluminio sin comprar otras piezas.

Funcionan, aunque no son fáciles de ensamblar.

Los modelos actuales están en `freecad_base_models/` (ver descripciones más abajo).

Todos estos modelos pueden ser reemplazados por partes de metal estándar:

![metal_parts.png](./images/metal_parts.png)

## 2020 coupling, joining and M3 slider nut

FreeCAD models:

* [2020.FCStd](./freecad_base_models/2020.FCStd)
* [2020_drop_in_nut.FCStd](./freecad_base_models/2020_drop_in_nut.FCStd)
* 

[Exported models](./freecad_base_models/exported_models/):

* [2020_slider_nut_tol02.stl](./freecad_base_models/exported_models/2020_slider_nut_tol02.stl)
* [2020_slider_nut.stl](./freecad_base_models/exported_models/2020_slider_nut.stl)
* [2020_socket_tol02-with_ears.stl](./freecad_base_models/exported_models/2020_socket_tol02-with_ears.stl)
* [2020_drop_in_nut-2020_slider_nut_drop_in.stl](./freecad_base_models/exported_models/2020_drop_in_nut-2020_slider_nut_drop_in.stl)

Estos modelos sirven para fijar extremos de perfiles 2020 a otras cosas (por ejemplo a otros perfiles 2020, usando el slider nut).

Nota post-printing: para que anden bien antes hay que meter tuercas M3 en los slots correspondientes (flechas rojas).

![2020_base_and_slider_nut.png](./freecad_base_models/images/2020_base_and_slider_nut.png "2020_base_and_slider_nut.png")

> Regular style T-slot nut, and coupling.

![drop_in_2020_nut.png](./freecad_base_models/images/drop_in_2020_nut.png)

> Drop-in style T-slot nut.

![joining-plates-2020.png](./freecad_base_models/images/joining-plates-2020.png)

> Joining plate (for a 3-ends corner, hence the M4 nut).

![angle.png](./freecad_base_models/images/angle.png)

> I have not tested this one as much.

Pictures of some _makes_:

![pasantes](images/03-pasantes_2020.png "pasantes")

> Very cute!

![02-acople_2020_3030.png](./images/02-acople_2020_3030.png)

> These did not turn out as stiff as I'd liked.

## 3030 M4 slider nut

FreeCAD file:

* [3030_nut_adapter.FCStd](./freecad_base_models/3030_nut_adapter.FCStd)

Exported models:

* [3030_slider_M4nut.stl](./freecad_base_models/exported_models/3030_slider_M4nut.stl)

Slider nut "casero" para perfiles 3030. No los necesitamos porque actualmente estamos usando solamente perfiles 2020.

Nota post-printing: para que anden antes hay que meter tuercas M4 en los slots correspondientes (flechas rojas).

![3030_slider_nut.png](./freecad_base_models/images/3030_slider_nut.png)


# Structural frame

Version 2 models are in a different directory, found at [modelos_XYZS/freecad_base_models](../modelos_XYZS/freecad_base_models) and the [alu_frame](../modelos_XYZS/freecad_base_models/alu_frame) subdirectory. See it's [README.md](../modelos_XYZS/freecad_base_models/alu_frame/README.md).

These models were made just to verify the dimensions of each part before cutting them to final size (they are not meant for 3D-printing).

Parts:

* 2020 V-slot aluminium profiles (500 mm pieces, cut to length).
* 9 mm laminated wood (with machined holes and chamfers).

![alu_frame2_as3.png](../modelos_XYZS/freecad_base_models/alu_frame/images/alu_frame2_as3.png)


# Older models

Los modelos viejos están en la carpeta [tinkercad_assembly_exports](./older_models/tinkercad_assembly_exports), ver su [README.md](./older_models/tinkercad_assembly_exports/README.md).
