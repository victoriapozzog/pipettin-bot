# Reference models

> From the Jubilee project's toolchange interface

There seems to be no immediate way of fiting this exact interface onto the Z carriage of the pipetting bot.

The problem is that one of the pairs of rods will not be as well supported (because the carriage has no structure in the center).

The interface is also rather "small" for my taste. But perhaps there can be two coupling sizes: the standard 51.96 mm triangle for Jubilee tools, and a "larger" triangle for micropipettes.

One alternative is to add the interface on the other side of the carriage (i.e. a carriage facing to the other side). In this way, i could keep the existing interface for pipettes,
_and_ support jubilee tools. Homing safetly may become an issue.

![pic.png](./images/pic.png)

## Important dimensions

For the tool-side of the interface see:

- Wiki page: https://jubilee3d.com/index.php?title=Tools#Creating_a_Custom_Tool

For the "head" side of the interface see the Jubilee models:

- Compressed STEP: [jubilee_xy_frame_assembly_Centered Carriage-jubilee.stpZ](./jubilee_models/jubilee_xy_frame_assembly_Centered Carriage-jubilee.stpZ)
- FreeCAD: [jubilee_xy_frame_assembly_Centered Carriage.FCStd](./jubilee_models/jubilee_xy_frame_assembly_Centered Carriage.FCStd)

### Tool-plate

Equilateral triangle with side length 51.96 mm.

Weird choice. It's not nicer in inches.

However this is not a strict length, i imagine +/- something will still work. 52 mm for example.

![triangle.png](./images/triangle.png)

### Carriage plate

From the carriage plate (see below), the maximum radius is 35 mm and the minimum 25. These are nicer numbers.

Maximum triangle side length (with point ball) is 60.62 mm, minimum 43.30 mm. And guess what, the mean is 51.96.

So the support points are meant to be 30 mm away from a center point, and form an equilateral triangle.

> This was documented in the wiki…

![angletri.png](./images/angletri.png)

## FreeCAD ports

### Original tool-plate

Ported here: [models.FCStd](./models.FCStd)

![jubilee_tool_plate.png](./images/jubilee_tool_plate.png)

### KFL08 REL version (WIP)

Here: [kfl08_interface.FCStd](./kfl08_interface.FCStd)

![kfl08.png](./images/kfl08.png)

Para que un tornillo de paso 8 m avance 10 mm tiene que girar 360º + 360º/5 = 432º. Agarraría con menos que eso, imagino.

Para que tenga resto pondría 360+90=450, o sea una vuelta y un cuarto.

Si quiero un desplazamiento lineal de 50 mm (ponele) en 5/4 de vuelta, una vuelta tiene que tener un perímetro de 40 mm. Y eso da un diametro de 12.7324 mm (aprox).

Eso es apenas mayor al diámetro externo del rodamiento (de 11 mm):

![medida.png](./images/medida.png)

Así que quizás no tenga que hacer más que fijar el alambre o correa con los tornillos prisioneros al mismo shaft.

Mala noticia: no hay lugar en el KFL08 para hacer esto. No entra ni la cabeza de un tornillo M3. Podría usar un "K08" sin el "pillow block" pero no parece que se venda por separado.

La alternativa más brusca es fijar el KFL08 (o el KP08 que tengo) al carrito. Habrá que aumentar la distancia al eje Z un poco.

![alt.png](./images/alt.png)

Frente a eso podría poner una polea "finita" (menor a 5 mm?) hecha en 3D y fijada con tornillos a una la varilla roscada (y marcada para que se agarren bien):

![alt2.png](./images/alt2.png)

En vez de una polea podría usar lo que usa el REL de Jubilee:
- El cable es un "wire rope":
    - https://en.wikipedia.org/wiki/Wire_rope
    - https://www.mcmaster.com/catalog/129/1691/34235T26
- Y está envuelto en un "spring guide":
    - https://stocksprings.drtempleman.com/item/spring-guides/spring-guide/sg-088-020-120

Wire rope: _Extra-Flexible Low-Stretch Coated Small-Diameter Braided Wire—Not for Lifting_

![wire_rope.png](./images/wire_rope.png)

Spring guide: _Straight, flexible spring produced from round wire to specified dimensions. Close wound, meaning that there is no space between adjacent coils within the length of the spring._

![spring_guide.png](./images/spring_guide.png)

Si uso estas cosas quizas me ahorre problemas:

![rel_pulley.png](./images/rel_pulley.png)

Para achicar la altura de 8mm a 5 mm puedo intentar:
- Reemplazar el inserto por una tuerca (que es más chata) y sacarle 2 mm del lado de abajo.
- Reemplazar las tuercas cuadradas por hex, y sacarle 0.5 mm del lado de arriba.

Eso lo dejaría en 5.5 mm, bastante bien.

En vez de eso podría volver a la idea de la polea "en espiral", para que gire rapido al inicio y cierre con torque.

### Threaded rod version

With non-standard (weird) Kfl08 bearing and 8x4 THSL nut. The models I imported for these components do not match the ones I purchased for unkown reasons. Beware.

My bearing was larger than specified (i.e. STEP models from the internet). I had to enlarge the socket.

The "slim" version is no more. The head plate must be 8 mm thick.

Model: [kfl08_interface-shorter_nut.FCStd](./kfl08_interface-shorter_nut.FCStd)

![weird_kfl08_and_nut.png](./images/weird_kfl08_and_nut.png)

### Test 1

Models:

- [cartesian_frame-Z_carriage-Z_base-X_carriage-inverted.FCStd](../../all_models/cartesian_frame-Z_carriage-Z_base-X_carriage-inverted.FCStd)
- [kfl08_interface-shorter_nut.FCStd](./kfl08_interface-shorter_nut.FCStd)

![cartesian_frame-Z_carriage-Z_base-X_carriage-inverted-jubilee.png](./images/cartesian_frame-Z_carriage-Z_base-X_carriage-inverted-jubilee.png)
