# Models from the Jubilee project

All models in this directory (folder) were designed by the team behind Jubilee, and are distributed on their websites under the CC-BY 4.0 licence. Full information is available at their GitHub repos and organizations:

- https://github.com/Poofjunior/jubilee_xyz_probe_tool
- https://github.com/machineagency/jubilee_syringe_tool/
- https://github.com/machineagency/jubilee
- https://jubilee3d.com/

