# Pipe's Tool-changer

Based on the "Getit Printer", published at Thingiverse: https://www.thingiverse.com/thing:3014704

## Models and Documentation

Printable models here:

* [printable_models.STEP.zip](./exported_models/printable_models.STEP.zip) (zip-compressed STEP file).

A STEP file with the full assembly, including the tool-changer system, is available here:

* [Assembly Pipetadora.zip](./Assembly Pipetadora.zip)

An explanation of how the system works and general design information has been documented here:

* [Documentacion de ToolChanger.docx](./Documentacion de ToolChanger.docx)

![printables.png](./images/printables.png)

> Printable models.

![assembly.png](./images/assembly.png)

> The tool-changing system has two pairs of interhaces; one between the carriage and tool (held with magnets), and another between the tool and parking post (held by hanging).

# Alternate systems

Have a look at:

* [modelos_toolchanger](..)
    * [README.md](../README.md)
* [freecad_toolchanger](../../modelos_XYZS/freecad_toolchanger)
    * [README.md](../../modelos_XYZS/freecad_toolchanger/README.md)

# TO-DO

- [ ] Upload solidworks project.

