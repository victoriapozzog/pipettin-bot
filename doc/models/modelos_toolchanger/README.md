# Toolchanger models

A list of the tool-changer projects we found is available here: https://gitlab.com/pipettin-bot/pipettin-grbl/-/issues/1

We first considered using the Jubilee toolchanger, but those models are rather complex, and better suited to a Z axis on the baseplate (instead of the toolhead). A lot of remodelling and programming would have been necessary, so we are considering other design ideas.

## Implementations

Two ideas have been implemented, and are currently being tested.

### Pipe: GetitPrinter

This idea has been implemented and documented. Have a look at the [pipes_toolchanger](./pipes_toolchanger) directory and its [README.md](./pipes_toolchanger/README.md).

Ver issue 19: https://gitlab.com/pipettin-bot/pipettin-grbl/-/issues/19

![toolchanger_screenshot_getit.png](./images/toolchanger_screenshot_getit.png)

> Pipe's toolchanger. TL-DR: Un pituto se mete entre las piezas que queremos separar.

### Nico: Push latch and magnets

In this mechanism, a "push latch" mechanism holds the tool on the toolpost. The tool carriage then pushes the tool against it, releasing it, and moves away magnetically bound to the tool.

This idea has been implemented, and has been documented here: [freecad_toolchanger](../modelos_XYZS/freecad_toolchanger). Have a look at the [README.md](../modelos_XYZS/freecad_toolchanger/README.md) file located at that directory.

![toolchanger_idea_magnets_and_push_latch_v1.png](../modelos_XYZS/freecad_toolchanger/images/toolchanger_idea_magnets_and_push_latch_v1.png)

> Tool-changer assembly.

### Mechanism implementation

![minilatch.png](./images/minilatch.png)

> The "mini latch".

A "push latch" from Thingiverse: https://www.thingiverse.com/thing:4544557/makes
- This one jammed easily. 

Another "push push switch" in Thingiverse: https://www.thingiverse.com/thing:81082
- This one is inspiring. The tool could simply have an equivalent to the flexible "lever" in [the video](https://youtu.be/7lCes1UMNlE?t=5), and the locking path on the toolpost. The linear movement would be constrained by the alignment rods, removing the need for an enclosure.

Another "touch latch" from GrabCAD: https://grabcad.com/library/mc-37-touch-latch/details

# Older ideas

These ideas are inactive for now.

## Renan: 2020 profile and Vslot wheels

See comments at issue 18 : https://gitlab.com/pipettin-bot/pipettin-grbl/-/issues/18#note_1019851549

Fix a segment of a 2020 v-slot profile to either the Z-axis or the tool.

Use v-slot wheels to dock to the profile.

A lock mechanism then "clicks", fixing the position of the profile relative to the wheels, which ends the docking sequence.

The parking sequence requires releasing the lock. It is slid out of place by colliding with a "peg" before parking.

## Nico: solenoid lock and magnets

Mismo proncipio que lo anterior, imanes del lado del toolhead, y una traba magnética del otro lado.

![cerradura_solenoide.png](./images/cerradura_solenoide.png)

> A solenoid door lock.

Si queremos reusar otra parte, están los solenoides para cierre centralizado autos (idea de INGIA):

![solenoide_auto_centralizado.png](./images/solenoide_auto_centralizado.png)

> A car's door electric locking/unlocking solenoid. Cheap and robust.
