# Deprecated models

Deprecated designs:

* [cooling_fan_holder](./cooling_fan_holder): A 3D-printable base and cooler fan holder for the Arduino CNC shield v3.0.
* [original_RPi_caseMulti](./original_RPi_caseMulti): A frame-style case for the Raspberry Pi 4 (access to the GPIO header is a bit awkward, not recommended).
* [PCB_base](./PCB_base): I tried to gather extra electronics on one base but this design turned out bad.
* [stack_for_RPi_Ardu_PCB](./stack_for_RPi_Ardu_PCB): I tried to gather stack the Arduino and Raspberry on top of each other but the design turned out bad.
* [end_stop_base.FCStd](./modelos_end_stop/freecad_base_models/end_stop_base.FCStd): Alternative base part for the mechanical end stops.

![end_stop_base.png](./modelos_end_stop/freecad_base_models/images/end_stop_base.png)


![arduino_base_and_cooler.png](./cooling_fan_holder/images/arduino_base_and_cooler.png)




