
# Cable carriers

Concept: https://en.wikipedia.org/wiki/Cable_carrier

![01-pasacables.png](./images/small/01-pasacables.png)

Contents:

[[_TOC_]]

# FreeCAD port of the cable carriers

Este proyecto de FreeCAD es un "port" o adaptación del modelo hecho en OpenSCAD [por Gasolin](https://www.thingiverse.com/thing:4375470/files), publicado en thingiverse.

A continuación se describe el contenido de los archivos más importantes.

## Model exports

Find your exported models here:

* [cable_carrier_link-assembly3-cable_carrier_link.stl](./exported_models/cable_carrier_link-assembly3-cable_carrier_link.stl)
* [cable_carrier-assembly3.stl](./exported_models/cable_carrier-assembly3.stl)

## [cable_carrier.FCStd](./cable_carrier.FCStd)

Tiene las piezas básicas que componen el pasacables (amarillo), las "bases" que anclan los pasacables del eje XYZ al resto de la estructura (verde), y las partes básicas para anclar los pasacables de la herramienta (azul, ver más abajo el assembly).

![cable_bases_and_basic_connectors.png](./images/cable_bases_and_basic_connectors.png)

En una actualización reciente se agregaron slots para fijar las bases a las varillas de 12 mm con tuercas prisioneras (rojo), y un corte para que la base más grande no choque con el stepper en la máquina ensamblada (azul). Nota: solo una de las bases marcadas en rojo necesita el slot, para estar fija sobre la varilla de 12 mm, la otra base tiene que quedar libre para deslizarse sobre la varilla junto con el carrito del eje X.

![cable_bases.png](./images/cable_bases.png)

## [cable_carrier_link-assembly3.FCStd](./cable_carrier_link-assembly3.FCStd)

Tiene el enlace básico ensamblado con Assembly3, con su "clip" para que los cables queden dentro.

![assembly3_top.png](./images/assembly3_top.png)

Agregué un [_fillet_](https://wiki.freecadweb.org/PartDesign_Fillet) a la parte "macho" del pasacables (flecha roja abajo), para mitigar el problema derivado de artefactos de la impresión  (elephants foot o de primera capa demasiado ancha). En resumen: se traban las piezas porque las primeras capas del macho y hembra a veces salen demasiado anchas y se chocan al conectarlas, haciendo que se trabe el movimiento.

![assembly3_bot.png](./images/assembly3_bot.png)


## [cable_carrier-assembly3.FCStd](./cable_carrier-assembly3.FCStd)

Archivo con ensambles en Assembly3. Se suma a lo anterior el par de bases para los pasacables del eje de la herramienta, armados pero no ensamblados con la base (nota: esas últimas bases están ensambladas en otro archivo también, ver más abajo).

![cable_carrier-assembly3.png](./images/cable_carrier-assembly3.png)


## [cable_carrier_bearing_seats-assembly3.FCStd](./cable_carrier_bearing_seats-assembly3.FCStd)

Tiene dos posibles bases para los pasacables de la herramienta, ensamblados con Assembly3. Estas piezas anclan el pasacables a la estructura de la máquina, pero permitiendo que roten usando rodamientos 625z.

**Nota**: no se dibujan los rodamientos 625z que van en cada base (flechas amarillas) ni los tornillos y arandelas que los mantienen unidos a las bases del cable carrier (flechas rojas o azules, para el conector macho y hembra, respectivamente).

![cable_bearing_bases_assembly3.png](./images/cable_bearing_bases_assembly3.png)


# Changelog

## 22-12-08: invert X-axis cablecarrier base

La idea es que quede apoyado el cablecarrier igual que en el primer robot. Ahora como que queda "parado" a 45º y no ocupa lugar al pedo. El tema viene por otro lado tmb, esta segunda tanda de pasacables e smas rigida y ademas dentro estan las sheats helicoidales.

![invertCCbaseX.png](./images/changelog/invertCCbaseX.png)

Base X con el otro socket:

- Creo que están mal orientados las bases igual… damn :/

![base_x_macho.png](./images/changelog/base_x_macho.png)

# Deprecated models

[cable_carrier-assembly.FCStd](./cable_carrier-assembly.FCStd): tiene las piezas fusionadas y ordenadas.

![cable_bases_assemblies.png](./images/cable_bases_assemblies.png)

> Assembly.

# Old notes

Antecedentes:

* Esta tiene infinitos likes, parece que es la que va: https://www.thingiverse.com/thing:1078216
* https://www.thingiverse.com/thing:2125076
* https://www.thingiverse.com/thing:204791
* Pero me gustó más esta: https://www.thingiverse.com/thing:4375470

## Models from Thingiverse

Thing 4375470 (chain_link_v2-Clip.scad) at https://www.thingiverse.com/thing:4375470

### Model issues

We tried tolerances 0.1, 0.2 and 0.3. Unfortunately all presented problems:

  * 0.1 was too small, and movement was impaired.
  * 0.2 and 0.3 were too loose, allowing bending of the chain in the wrong direction.

#### Chain clip problem

The original clips fit too tight.

Placing the clip deforms the model a little so that the hinges move more freely.  A slowly deforming clip will eventually fail, and the hinges may become lodged again. So putting less stress on that part (i.e. by making it wider or ncreasing tolerance) seems prudent.

#### Solution

Since no compromise in "tolerance" was good enough, we modified the OpenSCAD model to fit our needs:

  * Setting `under_angle=-4;` will fix the tolerance problem (line 29) producing chains that do not bend in the opposite direction.
  * Added a `clip_extra_width = 0.5;` and `clip_extra_tolerance=0.05` parameters to relax the clip a little (line 44 and 45).
  * several other patches are mentioned below...

#### Patches and explanation

Setting `under_angle=-4;` in the original version of the model caused some weird artifacts to appear. We removed them by subtracting two cubes in the seemingly "right" places (lines 98 and 126).

Also, this setting made the base of the chain detach from the baseplane in the original model. While this might have been intentional, it has two problems:

  * Makes printing harder.
  * Causes the links to lock less tightly, in a way that the "under angle" is less than expected (i.e. when setting `under_angle=-4;` you actually get less).

To fix this I added two blocks of the right height at the base of each side of the chain module (line 66 and 67). 

----

Added a 0.005 offset and enlarged one of the cubes in the `outline` module, which removed a strange visual artifact (line 73).

----

The clip tolerance was applied to the slot, but not to the clip. While this may have been intentional now there is a `clip_extra_tolerance=0.05` setting for that (line 45; the default used to be 0).

----

There was a small notch in the material that appears when setting `under_angle=-4;`. This is due to rotation of the cube at line 172. I moved the rotation function so that it only affects the `middle_0` module, and not the cube at line 172.

## Printing notes

An accurate first layer is important in this model to prevent bending of the chain in the wrong direction.

Squashed frist layer or elephants foot must be avoided. If the first layers print wider than expected, the hinges will become stuck on assembly. On the other hand, if they are smaller the chain will bend in the wrong direction more easily.

Proper calibration of the first layer height and/or using elephant's foot compensation may be important.

## Post-printing

Adding some graphite dust can help with lubrication and extend durability of the hinges.
