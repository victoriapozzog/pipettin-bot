# 2020 Aluminium frame

The base may be a 54x46 cm plank a grid of holes, for mounting objects on top (later on).

Files:

* [alu_frame_2020.FCStd](./alu_frame_2020.FCStd): basic models of the "2020" aluminium profile, and baseplate.
* [alu_frame_2020_assembly3.FCStd](./alu_frame_2020_assembly3.FCStd): new Assembly3 assembly.
* [alu_frame_2020_assembly.FCStd](./alu_frame_2020_assembly.FCStd): deprecated A2plus assembly.

I made an "Assembly3" assembly using the FreeCAD weekly AppImage: `FreeCAD_weekly-builds-29485-2022-07-11-conda-Linux-x86_64-py310.AppImage`.

This is the current model:

* The baseplate is a 9 mm wook plank, with a 50 mm chamfer.
* It should be 50 cm x 50 cm, but I messed up the order and it turned out smaller.

![alu_frame2_as3.png](./images/alu_frame2_as3.png)

There is an extra profile below the baseplate, for extra rigidity:

![profile_lengths.png](./images/profile_lengths.png)
