 
# XYZ frame parts

Parts for a cartesian CNC frame with:

* belt driven X/Y axis for speed,
* and a Z axis is driven by a lead screw for strength.

Contents:

[[_TOC_]]

## Exported models

Find your exported models at [exported_models](./exported_models):

* All parts: [eje_XZ_assembly3_toolchanger.stl](./exported_models/eje_XZ_assembly3_toolchanger.stl)
    * Exported from [eje_XZ_assembly3_toolchanger.FCStd](./eje_XZ_assembly3_toolchanger.FCStd).
    * Note: it includes parts from FreeCAD files at other directories; it is partially redundant.
* Y axis base with cablecarrier base: [eje_XZ_assembly3_toolchanger_baseY_cablecarrier.stl](./exported_models/eje_XZ_assembly3_toolchanger_baseY_cablecarrier.stl)
    * Exported separately because it splits in PrusaSlicer.

## General models

Old and new exported models for printing can be found at the [exported_models](./exported_models) directory.

### Base models

Individual models for all variants of the machine (both single-pipette and tool-changing machines).

FreeCAD file: [eje_XYZ.FCStd](./eje_XYZ.FCStd)

![eje_XYZ.png](./images/eje_XYZ.png)

### Cable carrier models

Printable models for the cable carriers and corresponding "base" parts.

See: [README.md](./freecad_cablecarriers/README.md) at [freecad_cablecarriers](./freecad_cablecarriers).

![01-pasacables.png](./freecad_cablecarriers/images/small/01-pasacables.png)

### Steel rods

These are not printable, but used in the assemblies with an illustrative purpose.

See: [README.md](./steel_rods/README.md) at [steel_rods](./steel_rods).

![screenshot.png](./steel_rods/screenshot.png)

### Aluminium base structure

Also not printable, and for illustrative purposes in the assembly files.

See: [README.md](./alu_frame/README.md) at [alu_frame](./alu_frame).

![alu_frame2_as3.png](./alu_frame/images/alu_frame2_as3.png)

## Assemblies

These assemblies were made for convenience, and are used in other mode general assemlies.

Most parts are common to both machine variants, except for the Z-axis carriage.

### XY-axis

Assembly of XY-axis parts, and of the old Z axis (for the single-pipette machine).

FreeCAD assembly3: [eje_XYZ_assembly3.FCStd](./eje_XYZ_assembly3.FCStd)

![eje_XYZ_assembly3.png](./images/eje_XYZ_assembly3.png)

> Part assembly using the assembly3 plugin (with freecad 0.2).

Note that it also includes:

* The GT2 belt locks (in cyan), modeled here: [modelos_racks_GT2](../modelos_racks_GT2).
* The GT2 idlers (in magenta), modeled here: [modelos_poleas_GT2](../modelos_poleas_GT2).


### Z-axis

Assembly of the new Z axis (for the tool-changing machine).

Assembled axis: [eje_XZ_assembly3_toolchanger.FCStd](./eje_XZ_assembly3_toolchanger.FCStd)

> Note: the YX axis parts have been linked from [eje_XYZ_assembly3.FCStd](./eje_XYZ_assembly3.FCStd).

![eje_XZ_assembly3_toolchanger.png](./images/eje_XZ_assembly3_toolchanger.png)


# Deprecated parts

## Belt tensioner base

The "belt tensioner base" is here: [eje_X_belt_tensioner.FCStd](./eje_X_belt_tensioner.FCStd)

This models is now part of [eje_XYZ.FCStd](./eje_XYZ.FCStd).

![eje_X_belt_tensioner.png](./images/eje_X_belt_tensioner.png)


## Old belt locks

The belt lock "lids" at [trabitas_gt2.FCStd](./trabitas_gt2.FCStd) have been replaced by files here: [modelos_racks_GT2](../modelos_racks_GT2) (see its [README.md](../modelos_racks_GT2/README.md)).

![trabitas_gt2.png](./images/trabitas_gt2.png)

# Alternate models

## V-slow wheel structure

Se podría usar el mismo perfil de aluminio como guía.

![gantry_plate.png](./images/gantry_plate.png)

Haría falta cambiar varias cosas.

Acá hay una opinión útil: https://www.reddit.com/r/3Dprinting/comments/5e77x3/comment/daab4f0/?utm_source=share&utm_medium=web2x&context=3

* Hay que ysar "eccentric spacers" para poder ajustar bien el sistema de rueditas:
    * https://www.youtube.com/watch?v=uMes1My5y5I
    * https://www.youtube.com/watch?v=pMtHy7sDNG4
* Linkean a [esta impresora](https://www.thingiverse.com/thing:1038673) de Openbuilds como referencia de "buena implementación" del sistema V-slot.

Acá hay modelos útiles:

* https://www.printables.com/es/social/160606-daniel-mundt/collections/91042
* https://www.printables.com/es/social/218355-bob/collections/168026
* https://www.printables.com/es/social/87391-ryan/collections/224495
* https://www.printables.com/es/social/201388-nielsw/collections/114917
* https://www.printables.com/es/social/355733-ryno-kotze/collections/258700

Quizás podría agarrar un CNC ya armado para no tener que modelar nada.

Por ejemplo:

* Tevo tornado https://www.thingiverse.com/thing:2834212
    *  Hay que agregar otro perfil al eje X.
    *  Cambiar el eje Y porque usa perfiles 2040.
    *  En teoría podría atornillar dos perfiles 2020 para no tener que comprar eso.
*  Rat-rig 2: https://www.youtube.com/watch?v=8LMkSbVEf_c
    *  https://www.thingiverse.com/thing:2970162
    *  Usa perfiles 2020 solamente.
    *  La versión 3 usa perfiles 3030 que son mas caros.
    *  Hay que agregar otro perfil al eje X igual que para la anterior.

![tevo_tornado.png](./images/tevo_tornado.png)
> Tevo tornado

![rat_rig2.png](./images/rat_rig2.png)

> Rat Rig 2

Misc:

* Convertir un perfild e aluminio "T" en "V-slot": https://www.youtube.com/watch?v=wkgvIPiVL7M