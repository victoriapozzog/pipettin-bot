# GT2 rack and belt lock

OpenSCAD file published by `kilothing` at [Thingiverse](https://www.thingiverse.com/thing:3096141/files) (see [LICENSE](./parametric_pulley/LICENSE)).

Model files:

* Customized OpenSCAD file: [timing_belts.scad](./timing_belts.scad)
* Rendered and exported in FreeCAD: [gt2_rack.FCStd](./gt2_rack.FCStd)

Model exports:

* Belt locks for the X axis: [gt2_rack-gt2_rack_and_lid.stl](./exported_models/gt2_rack-gt2_rack_and_lid.stl)
* Inverted slots for the Y axis: [gt2_rack-gt2_rack_and_lid_inverted.stl](./exported_models/gt2_rack-gt2_rack_and_lid_inverted.stl)

## Basic unit

Basic unit of the GT2 rack, 6mm width, 2mm length:

![unit.png](./images/unit.png)

## Rack

12 unit GT2 rack, total length 24 mm, width 6 mm:

![rack.png](./images/rack.png)

## Belt-lock assembly

Belt lock with lid for the X axis (left) and with inverted slots (right) for the Y axis:

![new_racks.png](./images/new_racks.png)

