# Binary files: [`bin`](./bin)

Any binary files we used would be here, but it's empty for now.

MongoDB files for the RPi on Raspbian _would_ be here, but they are huge. They are included in the OS image available for download below.

# mongodb 3.2.12 binaries

Mongo compiled for 32-bit Raspbian in the Raspberry Pi 4. Unfortunately, the apt version did not work out for us.

See notes at: [`gui/README.md`](../gui/README.md)

Compiling it took a loooong time, and this is why we distribute an image.

## Startup commands

    sudo mongod --storageEngine=mmapv1

### Repair unclean shutdown

    sudo mongod --storageEngine=mmapv1 --repair
    sudo mongod --storageEngine=mmapv1

## CLI shell

    mongod
