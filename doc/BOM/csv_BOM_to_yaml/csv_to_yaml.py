import yaml,csv,pprint
from unidecode import unidecode

csvfile = open('BOM.csv', 'r')
datareader = csv.reader(csvfile, delimiter=",", quotechar='"')
result = list()

fields = ["Name","Category","Description","Supplier","Notes","Link"]

content = dict()
for row_index, row in enumerate(datareader):
  print(row)
  if row_index == 0:
    continue
  else:
    # Create part heading and initialize empty attributes dict
    part_name_clean = row[0].replace(" ", "_").replace("-", "")
    # part_name_clean = part_name_clean.encode('ascii', errors='ignore').decode('ascii')
    part_name_clean = unidecode(part_name_clean)
    content[part_name_clean] = dict()
    # Iterate over the cells in the row
    for cell_index, cell in enumerate(row):
      print(cell)
      if cell_index == 0:
        continue
      else:
        # cell = cell.encode('ascii', errors='ignore').decode('ascii')
        content[part_name_clean][fields[cell_index]] = cell.strip()

pprint.pprint(content)

with open('BOM.yml', 'w') as outfile:
    yaml.dump(content, outfile, default_flow_style=False, allow_unicode=True)
