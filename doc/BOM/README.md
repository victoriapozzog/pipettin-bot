# BOM

Bill of materials (actually a shopping list) available here: [materiales_BOM_v5.ods](./materiales_BOM_v5.ods)

The contents below are a "work in progress", and are not ready for you. They will, eventually, hold descriptions of each part and their key charachteristics or alternatives.

# Proveedores

* PrintALot
* Mercadolibre https://mercadolibre.com.ar
* OpenHacks https://openhacks.com.ar
* INGIA https://ingia.com.ar/

Spreadsheet BOM, costos y proveedores:

* 2020/12: https://docs.google.com/spreadsheets/d/1nqJSYZ-nrdTaSJ7H5JYSDX2zimCQpnCKh9CfUzj1nbE/edit#gid=0
* 2022/06: [materiales_BOM_v5.ods](./materiales_BOM_v5.ods)

# Electro-Mecánica

## Estructura

- Partes impresas en PLA.
- Perfiles de aluminio.

TO-DO.

## Motores

Paso a paso.

* 5 x NEMA 17 con paso de 1.8°

## Poleas

Poleas GT2 de 10 mm de ancho (de correa) para los ejes XY.

* 3 x poleas dentadas 10mm de ancho (de correa) y 12mm de radio.
* 3 x poleas para tensar.

## Varillas lisas

De 8 mm de diámetro, para los ejes XY.

## Varillas roscadas y tuercas

Para los ejes Z y S.

* 2 x varillas
* 2 x tuercas

## Rulemanes lineales

Para las varillas de 8 mm de diámetro.

* 8 x rulemanes lineales de 15 mm de diametro externo.

TO-DO: longitud

## Rulemanes comunes

Para soportar la rotacion de la varilla roscada del eje Z.

* Rulemanes Abec 7 608zz

También los usé junto a cachitos de varilla lisa como reemplazo de poleas GT2 que me faltaban para tensar.

## Ventiladores de refrigeración

Para refrigerar los stepper drivers.

* 1 x Ventilador de 5 V ???

TO-DO: modelo

TO-DO: quizás sea reemplazable por un disipador grande o dos largos, especialmente si consideramos hacer un PCB adaptador entre el arduino y el 3.0 clone.

----

# Módulos para manejo de líquido

### Micropipeta

* 1 x Micropipeta Gilson p200

### Tips y descartes

- [ ] TO-DO

### Gradillas

- [ ] TO-DO

----

# Electrónica

## Controladores

* 1 x Raspberry Pi 4
* 1 x Arduino UNO

## CNC Shield y PCBs

- [ ] TO-DO.

* 1 x Arduino CNC shield clone (v3.0).
* ? x PCBs extra ???

## Sensores de fin de carrera

* 5 x ???

- [ ] TO-DO: modelo.
- [ ] TO-DO: Ojo con la compatibilidad con el CNC shield.

## Stepper drivers

* 5 x DRV8825 (o A4998).

## Fuentes

* 1 x Fuente 24 V DC, de 3A.
* 1 x Fuente DC step down (para bajar a 5V).

## Otros

### Capacitores

### Resistores

### Transistores
