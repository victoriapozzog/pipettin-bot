#!/usr/bin/env python
# See: https://stackoverflow.com/a/62983901/11524079
#      https://stackoverflow.com/a/65162874/11524079

import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

if __name__ == "__main__":
    setuptools.setup(
        name='newt',
        packages=['newt'],
        # packages=setuptools.find_packages()
        long_description=long_description,
        long_description_content_type="text/markdown",
        install_requires=['jsonschema', 'pydantic'],
    )
