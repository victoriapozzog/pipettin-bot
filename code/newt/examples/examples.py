import os, sys
module_path = os.path.expanduser("~/Projects/GOSH/gosh-col-dev/pipettin-grbl/newt/")
sys.path.append(module_path)

import newt

newt.contents.base_content()

newt.workspaces.EXAMPLE_WORKSPACE

newt.workspaces.base_workspace()

newt.protocols.base_protocol()

newt.protocol_actions.action_pick_tip()
