import time

def base_protocol(oid=None, name=None, date=None, description=None, 
                  workspace=None, actions=None,
                  meta="Generated by newt v1.0"):
    
    if date is None:
        date = time.asctime() + " UTC" + time.strftime("%z", time.localtime())
    
    protocol = {
        "_id": {"$oid": oid},
        "name": name,
        "date": {"$date": date},
        "description": description,
        "meta": meta,
        "workspace": workspace,
        "actions": []
    }
    
    if actions is not None:
        protocol["actions"].extend(actions)

    return protocol

base_protocol()


EXAMPLE_PROTOCOL = {
    "_id": {
      "$oid": "63c8bbe7528cad3ad585a035"
    },
    "name": "pcr a871 2023-01-19T03:41:27.038Z",
    "date": {
      "$date": 1674099687038
    },
    "description": "",
    "meta": "Generated by protocolBuilder v1.0",
    "workspace": "2023-01-18-prueba-pcr",
    "actions": [
      {
        "cmd": "HOME"
      },
      {
        "cmd": "PICK_TIP",
        "args": {
          "item": "200ul_tip_rack_MULTITOOL 1",
          "tool": "P200"
        }
      },
      {
        "cmd": "LOAD_LIQUID",
        "args": {
          "item": "5x16_1.5_rack_shorter 1",
          "selector": {
            "by": "name",
            "value": "water"
          },
          "volume": 51.84
        }
      },
      {
        "cmd": "DROP_LIQUID",
        "args": {
          "item": "5x16_1.5_rack_shorter 1",
          "selector": {
            "by": "name",
            "value": "level_mastermix"
          },
          "volume": 51.84
        }
      },
      {
        "cmd": "DISCARD_TIP",
        "args": {
          "item": "trash_bucket 1"
        }
      },
      {
        "cmd": "COMMENT",
        "args": {
          "text": "End step: step1"
        }
      },
      {
        "cmd": "HUMAN",
        "args": {
          "text": "Vortex and spin the tubes."
        }
      },
      {
        "cmd": "HOME"
      }
    ]
}
