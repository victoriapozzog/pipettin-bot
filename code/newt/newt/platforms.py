from copy import deepcopy

def base_platform(name="", description="", type="", color= "#cf947a", rotation=0, oid=""):
    data =  {
        "_id": {"$oid": oid},
        "name": name,
        "description": description,
        "type": type,
        "color": color,
        "rotation": rotation
    }
    return data

def platform_discard(name="", description="Generic discard container.", type="BUCKET", color= "#cf947a", rotation=0, oid="",
                     width=130, length=100, defaultTopPosition=60,
                     **kwargs):
     data = base_platform(name=name,description=description,type=type,color=color,rotation=rotation,oid=oid)
     # Generic spatial parameters
     data.update({
          "width": width,
          "length": length,
          "defaultTopPosition": defaultTopPosition
     })
     data.update(deepcopy(kwargs))
     return data

def platform_tip_rack(name="", description="Generic multitool tip rack.", type="TIP_RACK", color= "#8a1eF6", rotation=0, oid="",
                      width=122, length=86,
                      firstWellCenterX=10.5,
                      firstWellCenterY=10,
                      wellDiameter=4.5,
                      wellSeparationX=8.87,
                      wellSeparationY=8.87,
                      wellsColumns=12,
                      wellsRows=8,
                      defaultTopPosition=20,
                      defaultMaxVolume=160,
                      defaultLoadBottom={
                           "p200": 10,
                           "p20": 4
                      },
                      tipLength=50,
                      **kwargs):
     
     # Barebones platform
     data = base_platform(name=name,description=description,type=type,color=color,rotation=rotation,oid=oid)     
     # Generic spatial parameters
     data.update({
          "width": width,
          "length": length,
          "defaultTopPosition": defaultTopPosition
     })
     # Generic pipetting parameters
     data.update({
          "defaultMaxVolume": defaultMaxVolume,
          "defaultLoadBottom": deepcopy(defaultLoadBottom),
          "tipLength": tipLength
     })
     # 2D well plate grid parameters
     data.update({
          "firstWellCenterX": firstWellCenterX,
          "firstWellCenterY": firstWellCenterY,
          "wellDiameter": wellDiameter,
          "wellSeparationX": wellSeparationX,
          "wellSeparationY": wellSeparationY,
          "wellsColumns": wellsColumns,
          "wellsRows": wellsRows,
     })

     data.update(deepcopy(kwargs))
     return data

def platform_tube_rack(name="", description="Generic multitool tube rack.", type="TUBE_RACK", color= "#0c5b5c", rotation=0, oid="",
                       width=122, 
                       length=72,
                       firstWellCenterX=10,
                       firstWellCenterY=10,
                       wellDiameter=10,
                       wellSeparationX=13,
                       wellSeparationY=13,
                       wellsColumns=12,
                       wellsRows=5,
                       defaultTopPosition=20,
                       defaultMaxVolume=1500,
                       defaultBottomPositionTools={
                            "p200": 12,
                            "p20": 2
                       },
                       **kwargs):
     data = base_platform(name=name,description=description,type=type,color=color,rotation=rotation,oid=oid)
     # Generic spatial parameters
     data.update({
          "width": width,
          "length": length,
          "defaultTopPosition": defaultTopPosition
     })
     # 2D well plate grid parameters
     data.update({
          "firstWellCenterX": firstWellCenterX,
          "firstWellCenterY": firstWellCenterY,
          "wellDiameter": wellDiameter,
          "wellSeparationX": wellSeparationX,
          "wellSeparationY": wellSeparationY,
          "wellsColumns": wellsColumns,
          "wellsRows": wellsRows,
     })
     # Generic pipetting parameters
     data.update({
          "defaultMaxVolume": defaultMaxVolume,
          "defaultBottomPositionTools": deepcopy(defaultBottomPositionTools)
     })
     data.update(deepcopy(kwargs))
     return data


EXAMPLE_PLATFORM_TRASH_BUCKET =   {
    "_id": {
      "$oid": "5f03bd9d3f581412a6fefea3"
    },
    "name": "trash_bucket",
    "description": "Generic recipient",
    "type": "BUCKET",
    "color": "#cf947a",
    "width": 130,
    "length": 100,
    "defaultTopPosition": 60,
    "rotation": 0
  }

EXAMPLE_PLATFORM_TIP_RACK_MULTITOOOL =   {
    "_id": {
      "$oid": "631126028557e8163c9e3488"
    },
    "name": "200ul_tip_rack_MULTITOOL",
    "description": "Rack de tips",
    "type": "TIP_RACK",
    "color": "#8a1eF6",
    "width": 122,
    "length": 86,
    "firstWellCenterX": 10.5,
    "firstWellCenterY": 10,
    "wellDiameter": 4.5,
    "wellSeparationX": 8.87,
    "wellSeparationY": 8.87,
    "wellsColumns": 12,
    "wellsRows": 8,
    "defaultTopPosition": 20,
    "defaultMaxVolume": 160,
    "defaultLoadBottom": {
      "p200": 10,
      "p20": 4
    },
    "tipLength": 50,
    "rotation": 0
  }


EXAMPLE_PLATFORM_TUBE_RACK =   {
    "_id": {
      "$oid": "63c87744ae60cd60492301bf"
    },
    "name": "5x16_1.5_rack_shorter",
    "description": "Standard microtube rack",
    "type": "TUBE_RACK",
    "color": "#0c5b5c",
    "width": 215,
    "length": 72,
    "firstWellCenterX": 10,
    "firstWellCenterY": 10,
    "wellDiameter": 10,
    "wellSeparationX": 13,
    "wellSeparationY": 13,
    "wellsColumns": 12,
    "wellsRows": 5,
    "defaultTopPosition": 20,
    "defaultMaxVolume": 1500,
    "rotation": 0,
    "defaultBottomPositionTools": {
      "p200": 12,
      "p20": 2
    }
  }
