def base_action(cmd, args=None):
    action = {
        "cmd": cmd
    }
    if args is not None:
        action["args"] = args
    return action

def action_home(axis=None):
    if axis is None:
        action = base_action(cmd="HOME")
    if axis is not None:
        action = base_action(cmd="HOME", args = {"which": axis})
    
    return action

def action_pick_tip(item=None, tool=None):
    action = base_action(
        cmd="PICK_TIP", 
        args = {
          "item": item,
          "tool": tool
        }
    )
    return action

def action_discard_tip(item=None):
    action = base_action(
        cmd="DISCARD_TIP", 
        args = {
          "item": item
        }
    )
    return action

def action_comment(text=""):
    action = base_action(
        cmd="COMMENT", 
        args = {
          "text": text
        }
    )
    return action

def action_comment(text=""):
    action = base_action(
        cmd="COMMENT", 
        args = {
          "text": text
        }
    )
    return action

def action_load_liquid(volume=None, item=None, value=None, select_by="name"):
    action = base_action(
        cmd="LOAD_LIQUID", 
        args = {
          "item": item,
          "selector": {
            "by": select_by,
            "value": value
          },
          "volume": volume
        }
    )
    return action

def action_drop_liquid(volume=None, item=None, value=None, select_by="name"):
    action = action_load_liquid(volume=volume, item=item, value=value, select_by=select_by)
    action["cmd"] = "DROP_LIQUID"
    return action
