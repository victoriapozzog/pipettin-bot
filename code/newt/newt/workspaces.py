from copy import deepcopy

def base_workspace(oid="uniqueobjectid1234567890qwertyuiop", 
                   name="Barebones workspace",
                   description="Basic definition of an empty workspace.", 
                   items=[],
                   workarea_width=500,
                   workarea_height=350,
                   **kwargs):
    data = {
        "_id": {"$oid": oid},
        "name": name,
        "Description": description,
        "items": items.copy(),
        "dimensions": {
            # TODO: have the GUI use these parameters.
            "width": workarea_width,
            "height": workarea_height
        }
    }
    data.update(deepcopy(kwargs))
    return data


EXAMPLE_WORKSPACE = {
    "_id": {"$oid": "uniqueobjectid1234567890qwertyuiop"},
    "name": "Barebones workspace",
    "Description": "Basic definition of an empty workspace.",
    "items": [
        # An instance of a platform object.
        {   
            # ID of the platform
            "platform": "platform unique ID",
            # Name of the platform instance in this workspace.
            "name": "platform unique ID 1",
            # XY position of the platform instance in this workspace.
            "position": {              
                "x": 10,
                "y": -1
            },
            # List of the contents of the platform instance
            "content": [
                # A content item, of type "tip".
                {
                    "type": "tip",
                    "index": 1,
                    "name": "tip1",
                    "tags": [
                    "10ultip"
                    ],
                    "position": {
                    "col": 1,
                    "row": 1
                    }
                }
            ]
        },
        # An instance of a platform object of tipe "trash bucket".
        {
            "platform": "trash_bucket",
            "name": "My Trash 1",
            "position": {
            "x": 143,
            "y": -7
            },
            "content": []
        },
        # Tube rack platform instance
        {
            "platform": "5x16_1.5_rack",
            "name": "5x16_1.5_rack 1",
            "position": {
            "x": 330,
            "y": 231
            },
            "content": [
                {
                    "index": 1,
                    "maxVolume": 1500,
                    "name": "water",
                    "position": {
                    "col": 1,
                    "row": 1
                    },
                    "tags": [
                    "mixcomp",
                    "water"
                    ],
                    "type": "tube",
                    "volume": 51.84
                },
            ]
        }
    ]
}
