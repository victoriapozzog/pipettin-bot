
from copy import deepcopy
from jsonschema import validate

#### JSON schemas for contents ####

base_coordinate_xyz_schema = {
    "title": "Absolute position coordinate object, platformless.",
    "type" : "object",
    "properties": {
        "x": {"type": "number"},
        "y": {"type": "number"},
        "z": {"type": "number"}
    },
    "additionalProperties": False,
    "required": [ "x", "y", "z" ]
}

base_coordinate_colrow_schema = {
    "title": "Colmun-row based coordinate object, dependent on platform parameters for absolute position.",
    "type" : "object",
    "properties": {
        "col": {"type": "integer"},
        "row": {"type": "integer"}
    },
    "additionalProperties": False,
    "required": [ "col", "row" ]
}

# NOTE: "enum" restricts the possible values that a property can have.
content_type_schema = {"type" : "string", 
                       "description": "Content type definition", 
                       "enum": ["tube","tip","colony"]}

content_index_shcema = {"type" : "integer",
                       "minimum": 0}

content_tags_schema = {"type" : "array", "items": {"type": "string"}, "uniqueItems": True}

base_content_schema = {
   # "$schema": "https://json-schema.org/draft/2020-12/schema",
   # "$id": "https://example.com/product.schema.json",
   "title": "Definition of a single platform content.",
   "description": "A product in the catalog",
   "type" : "object",
   "properties" : {
        "type":      content_type_schema,
        "index":     content_index_shcema,
        "name":      {"type" : "string"},
        # NOTE: "oneOf" forces a property to be one of the options in an array.
        "position":  {"type" : "object",
                      "oneOf": [base_coordinate_colrow_schema]},
        "tags":      content_tags_schema
   },
   "required": [ "type", "position" ],
   "additionalProperties": False
}

base_content_schema_coords = {
   # "$schema": "https://json-schema.org/draft/2020-12/schema",
   # "$id": "https://example.com/product.schema.json",
   "title": "Platformless definition of a single content.",
   "description": "A product in the catalog",
   "type" : "object",
   "properties" : {
        # NOTE: "enum" restricts the possible values that a property can have.
        "type":      content_type_schema,
        "index":     content_index_shcema,
        "name":      {"type" : "string"},
        # NOTE: "oneOf" forces a property to be one of the options in an array.
        "coords":     {"type" : "object",
                      "oneOf": [base_coordinate_xyz_schema]},
        "tags":      content_tags_schema,
        "maxVolume": {"type": "number"},
        "tipLength": {"type": "number"},
        "volume":    {"type": "number"},
   },
   "required": [ "type", "coords", "maxVolume", "tipLength", "volume" ],
   "additionalProperties": False
}

# Validation examples:
# test_instance = {"type" : "tip", 
#                  "coords" : {"x": 10.1, "y": 10.9, "z": 10.2},
#                  "maxVolume": 160,
#                  "tipLength": 50.0,
#                  "volume": 0}
# validate(instance=test_instance, schema=base_content_schema_coords)
# validate(instance={"type" : "tip", "position" : {"col": 1, "row": 11}}, schema=base_content_schema)

#### Generic content generators ####

def base_content(type="", index=0, name="", position= {"col": 0, "row": 0}, tags=[]):
    """Base definition of a content, with platform-dependent position.

    Args:
        type (str, optional): Content type definition. Defaults to "".
        index (int, optional): Index for the conten in its box. Defaults to 0.
        name (str, optional): Name for the tip, such as "tip1" or "tube1". Defaults to "".
        position (dict, optional): Column and row of the tip. Defaults to {"col": 0, "row": 0}.
        tags (list, optional): Tags associated to this content. Defaults to [].

    Returns:
        dict: Content definition.
    """
    data =  {
                "type": type,
                "index": index,
                "name": name,
                "tags": deepcopy(tags),
                "position": deepcopy(position)
            }
    return data

def base_content_coord(type="", index=0, name="", maxVolume=200.0, tipLength=50.0, initial_volume=0.0, position={"x": 0, "y": 0, "z": 0}, tags=[]):
    """Base definition of a content, with absolute position.

    Args:
        type (str, optional): Content description. Defaults to "".
        index (int, optional): Index for the conten in its box. Defaults to 0.
        name (str, optional): Name for the tip, such as "tip1" or "tube1". Defaults to "".
        position (dict, optional): X, Y, and Z position of the tip. Defaults to {"x": 0, "y": 0, "z": 0}.
        tags (list, optional): Tags associated to this content. Defaults to [].

    Returns:
        dict: Content definition.
    """
    
    # Reuse the base content definition
    data = base_content(type=type, index=index, name=name, position=position, tags=tags)
    # Update it to contain 
    data.update({
        'maxVolume': maxVolume,
        'tipLength': tipLength,
        'volume': initial_volume
    })
    
    return data

#### Specific contents ####

def content_tube(index=1, volume=0, maxVolume=1500, name="water", position={"col": 1, "row": 1}, tags=[], type="tube", **kwargs):
    data = base_content(type="tube", index=index, name=name, position=position, tags=tags)
    data.update({
        "volume": volume,
        "maxVolume": maxVolume
    })
    data.update(deepcopy(kwargs))
    return data

def content_tip(index=1, maxVolume=200, name="tip1", position={"col": 1, "row": 1}, tags=[], type="tip", **kwargs):
    data = base_content(type=type, index=index, name=name, position=position, tags=tags)
    data.update({
        "maxVolume": maxVolume
    })
    data.update(deepcopy(kwargs))
    return data

#### EXAMPLES ####

EXAMPLE_CONTENT_TIP = {
    "type": "tip",
    "index": 1,
    "name": "tip1",
    "tags": [
        "10ultip"
    ],
    "position": {
        "col": 1,
        "row": 1
    }
}

EXAMPLE_CONTENT_TUBE = {
    "index": 1,
    "maxVolume": 1500,
    "name": "water",
    "position": {
        "col": 1,
        "row": 1
    },
    "tags": [
        "mixcomp",
        "water"
    ],
    "type": "tube",
    "volume": 51.84
}

EXAMPLE_CONTENT_TIP2 = {
    "type": "tip",
    "index": 1,
    "name": "tip1",
    "tags": [
        "10ultip"
    ],
    "position": {
        "x": 10,
        "y": 20,
        "z": 15
    }
}

EXAMPLE_CONTENT_TUBE2 = {
    "index": 1,
    "maxVolume": 1500,
    "name": "water",
    "position": {
        "x": 100,
        "y": 200,
        "z": 15
    },
    "tags": [
        "mixcomp",
        "water"
    ],
    "type": "tube",
    "volume": 51.84
}
