def base_platform_item(platform="", name="", position={"x": 10, "y": 10}, content=[]):
    data = {
        "platform": platform,
        "name": name,
        "position": {
            "x": position["x"],
            "y": position["y"]
        },
        "content": content.copy()
    }

    return data

EXAMPLE_PLATFORM_ITEM = {
    "platform": "5x16_1.5_rack",
    "name": "5x16_1.5_rack 1",
    "position": {
        "x": 330,
        "y": 231
        },
    "content": []
}
