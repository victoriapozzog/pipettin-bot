from copy import copy, deepcopy

def make_platform_item(platform, name="", position={"x": 10, "y": 10}, content=[], **kwargs):
    """Platform item from a X-Y position and the platform's definition.
    """
    data = base_platform_item(platform=platform["name"], name=name, position=position, content=content)
    data.update(kwargs)
    return data

def adjust_pos_by_first_item(platform_item, platform_definition, 
                             content_x=None, content_y=None, 
                             content_position={"col": 1, "row": 1}, 
                             tool_offset={"x": 0, "y": 0}):
    """
    Adjust the position of a platform item by the measured position of the first item.
    Calculate and replace the origin of a grid-type platform from the position of a content.
    """
    
    print("Original position:", platform_item["position"])
    
    # Look for a content with matching position
    contents = platform_item["content"]
    content = None
    for a_content in contents:
        if content_position == a_content["position"]:
            content = a_content
            break
    if content is None:
        print("No content matched, returning none.")
        return None

    if content_x is not None:
        x = content_x
        # Calculate X of platform origin compatible with measurement
        x -= (content["position"]["col"]-1) * platform_definition['wellSeparationX']
        x -= platform_definition['firstWellCenterX']
        # Apply the tool's offsets
        x -= tool_offset["x"]
        # Apply the adjustment
        platform_item["position"]["x"] = x

    if content_y is not None:
        # Calculate Y of platform origin compatible with measurement
        y = content_y
        y -= (content["position"]["row"] - 1) * platform_definition['wellSeparationY']
        y -= platform_definition['firstWellCenterY']

        # Apply the tool's offsets
        y -= tool_offset["y"]

        # Apply the adjustment
        platform_item["position"]["y"] = y

    print("Updated position:", platform_item["position"])
    
    return platform_item["position"]

def get_content(data, value=1, key="index"):
    """Returns the dictionary of a list with a matching key/value pair."""
    result = None
    for datum in data:
        if datum[key] == value:
            result = datum
    return result

def get_content_by_pos(platform_item, col, row):
    return get_content(platform_item["content"], value={'col': col, 'row': row}, key="position")

def add_content(content, platform_item, platform_definition,
                amount=1, name=None):

    # Default name for the inserted contents
    if name is None:
        name = content["name"]
    
    # Work on a full copy
    platform_item = deepcopy(platform_item)

    # Fill with content
    i=0
    n=amount
    for row in range(platform_definition["wellsRows"]):
        for col in range(platform_definition["wellsColumns"]):
            i+=1

            # Check if enough contents have been added
            if n < 1:
                break
            
            # Make name for the content
            if amount == 1:
                a_name = name
            else:
                a_name = name + str(i)

            
            # Skip existing indexes/col/pos/name
            if get_content(platform_item["content"], value={'col': col+1, 'row': row+1}, key="position"):
                continue
            if get_content(platform_item["content"], value=i, key="index"):
                continue
            if get_content(platform_item["content"], value=a_name, key="name"):
                continue
            
            # Add content to free slot
            a_content = deepcopy(content)
            a_content["position"] = {"col": col+1, "row": row+1}
            a_content["name"] = a_name
            a_content["index"] = i
            platform_item["content"].append(a_content)
            n -= 1

    if n > 0:
        print(f"Failed to add {n} contents. Not enough free/unique slots in platform item.")

    return platform_item


def fill_with_content(content, platform_item, platform_definition):

    # Look for a content with matching position, and stop if found.
    if len(platform_item["content"]) > 0:
        print("Preexisting content matched, returning none. Platform item contents must be empty.")
        return None
    
    # Work on a full copy
    platform_item = deepcopy(platform_item)

    amount = platform_definition["wellsRows"] * platform_definition["wellsColumns"]
    platform_item = add_content(content, platform_item, platform_definition,
                                name=content["type"], amount=amount)

    # Fill with content
    # i=0
    # for row in range(platform_definition["wellsRows"]):
    #     for col in range(platform_definition["wellsColumns"]):
    #         i+=1
    #         a_content = deepcopy(content)
    #         a_content["position"] = {"col": col+1, "row": row+1}
    #         a_content["name"] = content["type"] + str(i)
    #         a_content["index"] = i
    #         platform_item["content"].append(a_content)

    return platform_item

