# Klipper setup for the Pipetting Bot

Work in progress!

GitLab Issues:

* [# 47 - Replace GRBL with Klipper](https://gitlab.com/pipettin-bot/pipettin-grbl/-/issues/47)
* [# 17 - Firmware projects list](https://gitlab.com/pipettin-bot/pipettin-grbl/-/issues/17)

The objective is to use this setup:

![setup.png](./images/setup.png)

# Configuration

Updated configurations are available here:

- More general alternatives: [config](./klipper-stack/printer_data/config)
- Specific to this project: [configs](./configs)

## CNC-shield pin map

Useful for configuring Klipper to use the CNC-shield V3 (clone): [klipper_pin_config.svg](./klipper_pin_config.svg)

More detailed diagrams are available here: [doc/electronica](../../doc/electronica).

![klipper_pin_config.svg](./klipper_pin_config.svg)

Because of budget restrictions, we opted to use the "cloned" CNC shield and Arduino UNOs. If it is within your choices, please support the original developers and the improvement of their products by purchasing "original" open source hardware:

- CNC shield: https://blog.protoneer.co.nz/arduino-cnc-shield/
- Arduino UNO: https://store.arduino.cc/products/arduino-uno-rev3
- Pololu A4988 stepper driver: https://www.pololu.com/product/1182

# [GCODE](https://www.klipper3d.org/G-Codes.html)s

Klipper GCODE command reference, which follows Marlin's GCODE _closely_: https://www.klipper3d.org/G-Codes.html

Commands can be sent through an "API", read on for details, its great. The "controller agnosticity" is also wonderful.

Some examples:

* `STATUS`: Klippy status.

```
Send: STATUS
Recv: // Klipper state: Ready
```

* `QUERY_ENDSTOPS`: Get endstop trigger status.

```
Send: QUERY_ENDSTOPS
Recv: manual_stepper p20:TRIGGERED manual_stepper p200:TRIGGERED x:TRIGGERED y:TRIGGERED y1:TRIGGERED z:TRIGGERED
```

* `GET_POSITION`: Current position.

```
Send: GET_POSITION
Recv: // mcu: stepper_x:0 stepper_y:0 stepper_y1:0 stepper_z:0
Recv: // stepper: stepper_x:0.000000 stepper_y:0.000000 stepper_y1:0.000000 stepper_z:0.000000
Recv: // kinematic: X:0.000000 Y:0.000000 Z:0.000000
Recv: // toolhead: X:0.000000 Y:0.000000 Z:0.000000 E:0.000000
Recv: // gcode: X:0.000000 Y:0.000000 Z:0.000000 E:0.000000
Recv: // gcode base: X:0.000000 Y:0.000000 Z:0.000000 E:0.000000
Recv: // gcode homing: X:0.000000 Y:0.000000 Z:0.000000
Recv: ok
```

* `SET_STEPPER_ENABLE STEPPER=stepper_x ENABLE=1`: Enable stepper x.
* `SET_STEPPER_ENABLE STEPPER=stepper_x ENABLE=0`: Disable stepper x.
* `G28 X`: home the X axis.

Commands for "manual stepper":

* `MANUAL_STEPPER STEPPER=p20 SET_POSITION=0`
* `MANUAL_STEPPER STEPPER=p20 ENABLE=1`
* `MANUAL_STEPPER STEPPER=p20 MOVE=20`

There are, however, some caveats. Klipper is not a CNC project, it is meant for 3D-printers, and lacks some basic functionality. This is unfortunate, since its logic has overfitted to that particular use case, and the devs are "leery" towards extensions beyond FFD 3D-printing. Only the basic Marlin GCODEs are supported, and some modifications are necessary to add homing to the "extruder" axis (read on).

This is why the `klipper-for-cnc` fork exists.

# Klippy mods

Several modifications were made for compatibility, and are available at the GitLab fork: https://gitlab.com/pipettin-bot/forks/klipper

See: https://gitlab.com/pipettin-bot/pipettin-grbl/-/issues/47

## Extruder homing

Klipper can only home the XYZ axes (and un-synced "manual steppers").

We need synched and homeable extruders, in order to support "printing" on petri dishes with micropipettes.

Implemented at: [klipper](./klipper-stack/klipper)

## General probing

Klipper only probes in the Z-direction.

We need probing in all directions to support tip-probing, and tool-change probing.

Implemented at: [klipper](./klipper-stack/klipper)

# APIs

Klipper's API receives commands in JSON format: https://www.klipper3d.org/API_Server.html

There is also an HTTP API available, through the Moonraker program. It has been slightly modified to fit this project, and is published at: https://gitlab.com/pipettin-bot/forks/moonraker

## Piper module

A module to:

- Interact with Klipper (through the Moonraker HTTP API) and the Node GUI, using Python.
- Load protocol and workspace data from MongoDB.
- Convert GUI protocols to GCODE.
    - Save them to Klipper's `printer_data/gcode` directory (which can then be run via Mainsail).
    - Stream them to Klipper via Moonraker.

Have a look at the [README.md](./piper/README.md) under the [piper](./piper) directory.

## Usage examples

Examples on how to use it are available at the [notebooks](./piper/notebooks) directory, particularly at [examples_piper.ipynb](./piper/notebooks/examples_piper.ipynb).

The available API commands are listed here: https://www.klipper3d.org/API_Server.html#available-endpoints

All GCODEs in the [reference](https://www.klipper3d.org/G-Codes.html) can be sent, using the "script" API command.

