import pandas as pd
import numpy as np
from scipy.cluster.hierarchy import linkage, fcluster, dendrogram
from sklearn.preprocessing import LabelEncoder

# create a sample dataframe
data = {'Name': ['Alice', 'Bob', 'Charlie', 'Dave', 'Eve', 'Frank'],
        'Gender': ['Female', 'Male', 'Male', 'Male', 'Female', 'Male'],
        'Nationality': ['USA', 'USA', 'Canada', 'Canada', 'USA', 'Canada'],
        'Occupation': ['Student', 'Engineer', 'Engineer', 'Doctor', 'Student', 'Doctor'],
        'Salary': [20000, 50000, 45000, 70000, 25000, 80000]}
df = pd.DataFrame(data)

# convert categorical columns to numerical using label encoding
cat_cols = ['Gender', 'Nationality', 'Occupation']
le = LabelEncoder()
for col in cat_cols:
    df[col] = le.fit_transform(df[col])

# perform hierarchical clustering
Z = linkage(df[cat_cols], method='ward', metric='euclidean')

# set the maximum number of clusters to form
max_num_clusters = 3

# iterate over each level and assign cluster labels
for num_clusters in range(2, max_num_clusters + 1):
    labels = fcluster(Z, num_clusters, criterion='maxclust')
    col_name = f'Cluster_{num_clusters}'
    df[col_name] = labels

# print the results
print(df)