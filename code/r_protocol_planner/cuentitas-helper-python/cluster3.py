import pandas as pd
import numpy as np
from scipy.cluster.hierarchy import linkage, fcluster, dendrogram
from sklearn.preprocessing import LabelEncoder

import matplotlib.pyplot as plt

# create a sample dataframe
data = {'Name': ['Alice', 'Bob', 'Charlie', 'Dave', 'Eve', 'Frank'],
        'Gender': ['Female', 'Male', 'Male', 'Male', 'Female', 'Male'],
        'Nationality': ['USA', 'USA', 'Canada', 'Canada', 'USA', 'Canada'],
        'Occupation': ['Student', 'Engineer', 'Engineer', 'Doctor', 'Student', 'Doctor'],
        'Salary': [20000, 50000, 45000, 70000, 25000, 80000]}
df = pd.DataFrame(data)

# convert categorical columns to numerical using label encoding
cat_cols = ['Gender', 'Nationality', 'Occupation']
le = LabelEncoder()
for col in cat_cols:
    df[col] = le.fit_transform(df[col])

# perform hierarchical clustering
Z = linkage(df[cat_cols], method='ward', metric='euclidean')

# generate dendrogram
dendrogram(Z, labels=df['Name'].tolist())

# get the heights of each cluster
cluster_heights = Z[:,2]

# print the heights of each cluster
for i, height in enumerate(cluster_heights):
    print(f"Cluster {i+1} height: {height}")

# show the plot
plt.show()
