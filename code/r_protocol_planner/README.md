# R code for pipettin-grbl

The PCR Mix protocol planner uses a custom R script, which clusters tubes by similarity into a dendrogram.
The resulting tree is cut, and intermediate solutions are prepared, reducing the amount of required pipetting steps,
and a consequently faster protocol. 

Licenced under the terms of the [GNU Affero GPLv3 licence](https://gitlab.com/pipettin-bot/pipettin-grbl/-/blob/master/LICENSES.md#software) license.

We have this version of R on the Raspberry:

```
> R.version
               _
platform       arm-unknown-linux-gnueabihf
arch           arm
os             linux-gnueabihf
system         arm, linux-gnueabihf
status
major          3
minor          5.2
year           2018
month          12
day            20
svn rev        75870
language       R
version.string R version 3.5.2 (2018-12-20)
nickname       Eggshell Igloo
```

> R 3.5 is quite old, but this is what is packaged in `r-base` by our Raspbian buster distribution for the Raspberry Pi.

## PCRmix protocol planner

### Requirements

* R 3.5.2, from the raspbian apt repos: `sudo apt install r-base`
* Some packages: `install.packages(c("dendextend", "tidyr", "dplyr", "jsonlite", "cluster"))`

### Code and usage

```sh
# First argument is the input JSON, and the second is the output JSON path
Rscript --vanilla ~/pipettin-grbl/Rscripts/pcr_planner.R ~/pipettin-grbl/Rscripts/sample_input.json ~/pipettin-grbl/Rscripts/sample_output2.json
```

See `pcr_planner.R`.

