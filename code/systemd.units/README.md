# Systemd units for the Raspberry Pi

"Systemd units" handle the startup of the GUI's Node.js server, and of the Python driver module, and other services.

## Setup

> Setup has already been done for you in the ISO image.

### MongoDB unit

Copy and enable the database's systemd unit: [mongodb.service](./mongo/mongodb.service)

```bash
sudo cp ~/pipettin-grbl/systemd.units/mongodb.service /etc/systemd/system/mongodb.service
sudo systemctl daemon-reload
sudo systemctl enable mongodb.service
sudo systemctl start mongodb.service
sudo systemctl status mongodb.service
```

An example configuration file for mongodb is available here: [mongod.conf](./mongo/mongod.conf)

### User units

Configure systemd [lingering](https://wiki.archlinux.org/title/systemd/User#Automatic_start-up_of_systemd_user_instances), at least once as the `pi` user:

```bash
sudo loginctl enable-linger pi
```

Install the systemd Python module, required by the "commander" unit:

```bash
sudo apt-get install python-systemd python3-systemd
```

User unit definitions reside at `~/.config/systemd/user/` in the RPi's filesystem.

Create the directory, link the units, and reload the systemd user daemon:

```bash
# Link all service files
mkdir -p ~/.config/systemd/user/
ln -s /home/pi/pipettin-grbl/systemd.units/*.service ~/.config/systemd/user/

# Reload the user's units
systemctl --user daemon-reload
```

> Some status info is printed on login by bash (see `~/.bashrc`).

You may use standard systemctl commands to check on the services manually.

For example:

```bash
systemctl --user status nodegui.service
systemctl --user status commander.service
```

Handy bash aliases to restart the units are defined at `~/.bashrc`, such as:

```bash
# Restart units and follow logs
alias piper_restart='clear; systemctl --user restart piper.service; tail -f /tmp/moon.log -n1'  
alias piper_restart2='clear; systemctl --user restart piper.service; journalctl --user-unit piper -f'
alias gui_restart='systemctl --user restart nodegui.service; journalctl --user-unit nodegui -f'
```

## Node GUI

Located at: `systemd.units/nodegui.service`

For development, this file uses `nodemon` instead of `node`.

A compatible (legacy) nodemon version was installed with:

```bash
sudo npm install -g nodemon@2.0.12 # Details at https://github.com/remy/nodemon/issues/1948#issuecomment-953665876
```

Start and enable the unit:

```bash
systemctl --user start nodegui.service
systemctl --user status nodegui.service
systemctl --user enable nodegui.service
```

> Note: edit the unit file to use `node` instead of `nodemon` for deployments.

## Python3 commander

### piper (current)

Located at: `systemd.units/piper.service`

Start and enable the unit:

```bash
systemctl --user start piper.service
systemctl --user status piper.service
systemctl --user enable piper.service
```

### protocol2gcode (legacy)

Located at: `systemd.units/commander.service`

Learn more about it at: [`protocol2gcode/README.md`](../protocol2gcode/README.md)

Start and enable the unit:

```bash
systemctl --user start commander.service
systemctl --user status commander.service
systemctl --user enable commander.service
```

## Jupyter Lab

Unit file: [jupyter.service](./jupyter.service)

To be placed at `/etc/systemd/system/jupyter.service`.

```bash
sudo cp jupyter.service /etc/systemd/system/jupyter.service
sudo systemctl daemon-reload
sudo systemctl start jupyter.service
sudo systemctl status jupyter.service
sudo systemctl enable jupyter.service
```

## GitBuilding docs

Unit file: [gitbuilding.service ](./gitbuilding.service )

Requirements:

- docs repo at `/home/pi/pipettin-grbl-docs/`
- venv with gitbuilding installed at `/home/pi/pipettin-grbl-docs/bot_venv`
- Learn more at: https://gitlab.com/pipettin-bot/pipettin-grbl-docs

```bash
sudo cp gitbuilding.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl start gitbuilding.service
sudo systemctl status gitbuilding.service
sudo systemctl enable gitbuilding.service
```
