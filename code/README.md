# Code

Software for the pipettin-bot project.

For the sake of modularity, many components have been included as git submodules.

General code flow:

![code_flow.svg](./code_flow.svg)

### Protocol designer: [`pipettin-gui`](./pipettin-gui)

Source code for the Node.js GUI, written in the ReactJS framework (included as a submodule): https://gitlab.com/pipettin-bot/pipettin-gui

The original GUI, as designed and written in pure JS by Facundo, is available in the [archive branch](https://gitlab.com/pipettin-bot/pipettin-bot/-/tree/master-backup-models).

### CNC firmware: [`klipper`](./klipper)

The project has moved from GRBL to Klipper, but will stick to the Arduino UNO + CNC shield. Using more expensive boards is trivial with Klipper.

The directory contains general information relevant to the specific configuration for this project.

The full Klipper software stack has been forked and published over here:

- Meta-repo including all forks as submodules: https://gitlab.com/pipettin-bot/forks/klipper-stack
- All forks, including Klipper: https://gitlab.com/pipettin-bot/forks/

### Protocol translators and GCODE senders

#### [`piper`](./klipper/piper)

Source files for the main python module, responsible for:

- Parsing "actions" of "mid-level" protocols from the GUI into GCODE (with a few "GCODE extensions").
- Operates the hardware, by sending GCODE to Klipper through Moonraker.

#### [`newt`](./newt)

A small module (WIP) to generate JSON objects for workspaces, platforms, contents, and protocol steps.

The idea is to make JSON "schemas" for each kind of data.

See:

- https://gitlab.com/pipettin-bot/pipettin-bot/-/issues/119
- https://gitlab.com/pipettin-bot/pipettin-bot/-/issues/139

### Mix planner: [`r_protocol_planner`](./r_protocol_planner)

R scripts to compute pipetting steps for the automatic "PCR workspace builder" feature in the GUI.

They work based on hierarchichal clustering of the target sample composition or contidions, to find a short "path" of pipetting steps.

![dendro_planner.png](./r_protocol_planner/images/dendro_planner.png)

This is being migrated to python over here: [cuentitas-helper-python](./r_protocol_planner/cuentitas-helper-python)

### Startup scripts: [`systemd.units`](./systemd.units)

`systemd.units` files to start/restart the:

- Node.js server.
- Robot driver module.
- Klipper software stack.
- Jupyter Lab server.
- GitBuilding documentation server.

Units for the Klipper stack are available at the meta repo: https://gitlab.com/pipettin-bot/forks/klipper-stack

> Some peopple hate systemd, but I don't know enough Linux to realize why. :B
