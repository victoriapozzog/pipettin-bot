# Printable pipette parts

Printable parts: [printable_parts.3mf](./printable_parts.3mf)

![piepte_bruno_v1.png](./images/piepte_bruno_v1.png)

As extracted from the part assembly, in compressed STEP file format: [AS0100.stpZ](./AS0100.stpZ)

| Part Number | Quantity | Definition                            |
|-------------|----------|---------------------------------------|
| PZ0202      | 1        | Soporte Carro Movil                   |
| PZ0102-A    | 1        | Acople Vaina R20-1000 c/rosca interna |
| PZ0103      | 1        | Soporte Inferior                      |
| PZ0105      | 1        | Soporte Motor                         |
| PZ0108      | 1        | Anillo trampa sello                   |

![pic.png](./images/pic.png)

# Eyector

> WIP: https://gitlab.com/pipettin-bot/pipettin-bot/-/issues/140
![eyector_v3.svg](./images/eyector_v3.svg)