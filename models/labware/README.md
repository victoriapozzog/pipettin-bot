# Pipettin labware

- Well-plate (incubator): [well_plates](./well_plates)
- Orbital shaker: [orbital_shaker](./orbital_shaker)

Eye candy (WIP): [labware_asm.FCStd](./labware_asm.FCStd)

![asm.png](./images/asm.png)
