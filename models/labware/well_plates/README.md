# Well-plates

## Standard dimensions

See:

- https://en.wikipedia.org/wiki/Microplate#History
- SBS: https://www.slas.org/education/ansi-slas-microplate-standards/

![96-Well_plate.svg](./images/96-Well_plate.svg)

