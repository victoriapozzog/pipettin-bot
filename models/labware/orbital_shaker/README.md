 Orbital shaker mechanism models, based on standard bearings:
 
 - 608zz: allows > 12mm orbit diameter.
 - 625z: allows > 9 mm orbit diameter.
 - Less than 10 mm is recommended for microplates, and 3-5 mm seems standard.
 - To achieve 3 mm the eccentric couplers need to be redesigned.
 
 For details see: https://gitlab.com/pipettin-bot/pipettin-grbl/-/issues/122

Contents for reference:

- Main models: [shaker-625z-9mm-orbit.FCStd](./shaker-625z-9mm-orbit.FCStd)
- Assembly3: [assembly.FCStd](./assembly.FCStd)
 
 ![assembly.png](./images/assembly.png)
 
 