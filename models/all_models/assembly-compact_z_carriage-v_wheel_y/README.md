# Compact Z axis assembly

This variant increases the movement range of the Z axis, using a more "compact" design, and also puts load cells between the stepper and the axis, for probing.

Uses exported STEPs from:

- [cartesian_frame-Z_carriage-Z_base-X_carriage-inverted-load_cell-compact.FCStd](../cartesian_frame-Z_carriage-Z_base-X_carriage-inverted-load_cell-compact.FCStd)
- [cartesian_frame-X_base-Y_carriage_V_wheel-split_parts.FCStd](../cartesian_frame-X_base-Y_carriage_V_wheel-split_parts.FCStd)

![assembly-compact_z-v_wheel_y.png](./images/assembly-compact_z-v_wheel_y.png)

![assembly-compact_z-v_wheel_y-y-range.png](./images/assembly-compact_z-v_wheel_y-y-range.png)
