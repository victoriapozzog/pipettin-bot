# All models

List of pending tasks at [TODO.md](./TODO.md). Most are assembly tasks.

----

Table of contents:

[[_TOC_]]

-—

Table summarising contents:

| Module           | Submodule            | File                                                                                                                                                             | Description |
|------------------|----------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------|
| General assembly |                      | [assembly.FCStd](./assembly.FCStd)                                                                                                                               |             |
| Baseplate        | Assembly             | [baseplate-assembly3.FCStd](./baseplate-assembly3.FCStd)                                                                                                         |             |
| Baseplate        | Baseplate            | [baseplate-base_parts.FCStd](./baseplate-base_parts.FCStd)                                                                                                       |             |
| Electronics      | Cable management     | [cable_carrier-assembly3-bearing_seats.FCStd](./cable_carrier-assembly3-bearing_seats.FCStd)                                                                     |             |
| Electronics      | Cable management     | [cable_carrier-assembly3-chain_links.FCStd](./cable_carrier-assembly3-chain_links.FCStd)                                                                         |             |
| Electronics      | Cable management     | [cable_carrier-basic_parts.FCStd](./cable_carrier-basic_parts.FCStd)                                                                                             |             |
| Motion system    | Basic parts          | [cartesian_frame-2020_profiles-assembly3.FCStd](./cartesian_frame-2020_profiles-assembly3.FCStd)                                                                 |             |
| Motion system    | Basic parts          | [cartesian_frame-2020_profiles-drop_in_nut.FCStd](./cartesian_frame-2020_profiles-drop_in_nut.FCStd)                                                             |             |
| Motion system    | Basic parts          | [cartesian_frame-2020_profiles-joining-plate.FCStd](./cartesian_frame-2020_profiles-joining-plate.FCStd)                                                         |             |
| Motion system    | Basic parts          | [cartesian_frame-2020_profiles.FCStd](./cartesian_frame-2020_profiles.FCStd)                                                                                     |             |
| Motion system    | Cartesian kinematics | [cartesian_frame-assembly3-carriage_assemblies.FCStd](./cartesian_frame-assembly3-carriage_assemblies.FCStd)                                                     |             |
| Motion system    | Cartesian kinematics | [cartesian_frame-assembly3-xyz_assembly-inverted_z-v_wheel_y.FCStd](./cartesian_frame-assembly3-xyz_assembly-inverted_z-v_wheel_y.FCStd)                         |             |
| Motion system    | Cartesian kinematics | [cartesian_frame-assembly3-xyz_assembly-inverted_z.FCStd](./cartesian_frame-assembly3-xyz_assembly-inverted_z.FCStd)                                             |             |
| Motion system    | Cartesian kinematics | [cartesian_frame-assembly3-xyz_assembly.FCStd](./cartesian_frame-assembly3-xyz_assembly.FCStd)                                                                   |             |
| Motion system    | Cartesian kinematics | [cartesian_frame-tensioner-XY_belts.FCStd](./cartesian_frame-tensioner-XY_belts.FCStd)                                                                           |             |
| Motion system    | Cartesian kinematics | [cartesian_frame-X_base-Y_carriage_V_wheel-split_parts.FCStd](./cartesian_frame-X_base-Y_carriage_V_wheel-split_parts.FCStd)                                     |             |
| Motion system    | Cartesian kinematics | [cartesian_frame-X_base-Y_carriage_V_wheel.FCStd](./cartesian_frame-X_base-Y_carriage_V_wheel.FCStd)                                                             |             |
| Motion system    | Cartesian kinematics | [cartesian_frame-X_base-Y_carriage.FCStd](./cartesian_frame-X_base-Y_carriage.FCStd)                                                                             |             |
| Motion system    | Cartesian kinematics | [cartesian_frame-Y_base.FCStd](./cartesian_frame-Y_base.FCStd)                                                                                                   |             |
| Motion system    | Cartesian kinematics | [cartesian_frame-Z_carriage-Z_base-X_carriage-inverted-load_cell-cleanup.FCStd](./cartesian_frame-Z_carriage-Z_base-X_carriage-inverted-load_cell-cleanup.FCStd) |             |
| Motion system    | Cartesian kinematics | [cartesian_frame-Z_carriage-Z_base-X_carriage-inverted-load_cell-compact.FCStd](./cartesian_frame-Z_carriage-Z_base-X_carriage-inverted-load_cell-compact.FCStd) |             |
| Motion system    | Cartesian kinematics | [cartesian_frame-Z_carriage-Z_base-X_carriage-inverted-load_cell-shorter.FCStd](./cartesian_frame-Z_carriage-Z_base-X_carriage-inverted-load_cell-shorter.FCStd) |             |
| Motion system    | Cartesian kinematics | [cartesian_frame-Z_carriage-Z_base-X_carriage-inverted-load_cell.FCStd](./cartesian_frame-Z_carriage-Z_base-X_carriage-inverted-load_cell.FCStd)                 |             |
| Motion system    | Cartesian kinematics | [cartesian_frame-Z_carriage-Z_base-X_carriage-inverted.FCStd](./cartesian_frame-Z_carriage-Z_base-X_carriage-inverted.FCStd)                                     |             |
| Motion system    | Cartesian kinematics | [cartesian_frame-Z_carriage-Z_base-X_carriage.FCStd](./cartesian_frame-Z_carriage-Z_base-X_carriage.FCStd)                                                       |             |
| Electronics      | Enclosure            | [electronics-box-assembly3.FCStd](./electronics-box-assembly3.FCStd)                                                                                             |             |
| Electronics      | Enclosure            | [electronics-crude_switch_box.FCStd](./electronics-crude_switch_box.FCStd)                                                                                       |             |
| Electronics      | Support              | [electronics-end_stop-rod_fastener-8mm_and_12mm.FCStd](./electronics-end_stop-rod_fastener-8mm_and_12mm.FCStd)                                                   |             |
| Motion system    | GT2 timing belts     | [GT2-20T_idler-smooth.FCStd](./GT2-20T_idler-smooth.FCStd)                                                                                                       |             |
| Motion system    | GT2 timing belts     | [GT2-20T_idler.FCStd](./GT2-20T_idler.FCStd)                                                                                                                     |             |
| Motion system    | GT2 timing belts     | [GT2-belt_lock-thin.FCStd](./GT2-belt_lock-thin.FCStd)                                                                                                           |             |
| Motion system    | GT2 timing belts     | [GT2-belt_lock-with_base.FCStd](./GT2-belt_lock-with_base.FCStd)                                                                                                 |             |
| Motion system    | Basic parts          | [steel_rods-smooth.FCStd](./steel_rods-smooth.FCStd)                                                                                                             |             |
| Tools            | Tool-changer system  | [toolchanger-interface-jubilee-kfl08_nut.FCStd](./toolchanger-interface-jubilee-kfl08_nut.FCStd)                                                                 |             |
| Tools            | Micropipette adapter | [toolchanger-pipette-gilson-assembly3_adapter.FCStd](./toolchanger-pipette-gilson-assembly3_adapter.FCStd)                                                       |             |
| Tools            | Micropipette adapter | [tools-pipette-gilson-adapter_parts.FCStd](./tools-pipette-gilson-adapter_parts.FCStd)                                                                           |             |
| Tools            | Micropipette adapter | [tools-pipette-gilson-assembly3_adapter.FCStd](./tools-pipette-gilson-assembly3_adapter.FCStd)                                                                   |             |
| Tools            | Micropipette adapter | [tools-pipette-gilson-tip_probe.FCStd](./tools-pipette-gilson-tip_probe.FCStd)                                                                                   |             |
| Tools            | Tool-changer system  | [tools-toolchanger-pipette_adapter_extras.FCStd](./tools-toolchanger-pipette_adapter_extras.FCStd)                                                               |             |
| Tools            | Tool-changer system  | [tools-toolchanger-toolpost-assembly.FCStd](./tools-toolchanger-toolpost-assembly.FCStd)                                                                         |             |
| Tools            | Tool-changer system  | [tools-toolchanger-toolpost-dock.FCStd](./tools-toolchanger-toolpost-dock.FCStd)                                                                                 |             |
| Tools            | Tool-changer system  | [tools-toolchanger-toolpost-mini_latch.FCStd](./tools-toolchanger-toolpost-mini_latch.FCStd)                                                                     |             |
> Please edit [file_list.ods](./file_list.ods) and re-export table to this file.

## A visual directory

Screenshots below are named after the FreeCAD file containing the models it shows.

Click on the images to go to the corresponding FreeCAD file.

### V1 Motion: Upright Z axis, lm8uu Y axis

[assembly.FCStd](./assembly.FCStd)

[![assembly.png](./images/assembly.png)](./assembly.FCStd)

[cartesian_frame-tensioner-XY_belts.FCStd](./cartesian_frame-tensioner-XY_belts.FCStd)

[![cartesian_frame-tensioner-XY_belts.png](./images/cartesian_frame-tensioner-XY_belts.png)](./cartesian_frame-tensioner-XY_belts.FCStd)

[cartesian_frame-X_base-Y_carriage.FCStd](./cartesian_frame-X_base-Y_carriage.FCStd)

[![cartesian_frame-X_base-Y_carriage.png](./images/cartesian_frame-X_base-Y_carriage.png)](./cartesian_frame-X_base-Y_carriage.FCStd)

[cartesian_frame-Y_base.FCStd](./cartesian_frame-Y_base.FCStd)

[![cartesian_frame-Y_base.png](./images/cartesian_frame-Y_base.png)](./cartesian_frame-Y_base.FCStd)

[cartesian_frame-Z_carriage-Z_base-X_carriage.FCStd](./cartesian_frame-Z_carriage-Z_base-X_carriage.FCStd)

[![cartesian_frame-Z_carriage-Z_base-X_carriage.png](./images/cartesian_frame-Z_carriage-Z_base-X_carriage.png)](./cartesian_frame-Z_carriage-Z_base-X_carriage.FCStd)

[cartesian_frame-Z_carriage-Z_base-X_carriage-inverted.FCStd](./cartesian_frame-Z_carriage-Z_base-X_carriage-inverted.FCStd)

[![cartesian_frame-Z_carriage-Z_base-X_carriage-inverted.png](./images/cartesian_frame-Z_carriage-Z_base-X_carriage-inverted.png)](./cartesian_frame-Z_carriage-Z_base-X_carriage-inverted.FCStd)

[cartesian_frame-assembly3-carriage_assemblies.FCStd](./cartesian_frame-assembly3-carriage_assemblies.FCStd)

[![cartesian_frame-assembly3-carriage_assemblies.png](./images/cartesian_frame-assembly3-carriage_assemblies.png)](./cartesian_frame-assembly3-carriage_assemblies.FCStd)

[cartesian_frame-assembly3-xyz_assembly.FCStd](./cartesian_frame-assembly3-xyz_assembly.FCStd)

[![cartesian_frame-assembly3-xyz_assembly.png](./images/cartesian_frame-assembly3-xyz_assembly.png)](./cartesian_frame-assembly3-xyz_assembly.FCStd)

### V2 Motion: inverted Z-axis with load cells, V-wheel Y axis

[cartesian_frame-Z_carriage-Z_base-X_carriage-inverted-load_cell.FCStd](./cartesian_frame-Z_carriage-Z_base-X_carriage-inverted-load_cell.FCStd)

[![cartesian_frame-Z_carriage-Z_base-X_carriage-inverted-load_cell.png](./images/cartesian_frame-Z_carriage-Z_base-X_carriage-inverted-load_cell.png)](./cartesian_frame-Z_carriage-Z_base-X_carriage-inverted-load_cell.FCStd)

[cartesian_frame-X_base-Y_carriage_V_wheel-split_parts.FCStd](./cartesian_frame-X_base-Y_carriage_V_wheel-split_parts.FCStd)

[![cartesian_frame-X_base-Y_carriage_V_wheel-split_parts.png](./images/cartesian_frame-X_base-Y_carriage_V_wheel-split_parts.png)](./cartesian_frame-X_base-Y_carriage_V_wheel-split_parts.FCStd)

[cartesian_frame-Z_carriage-Z_base-X_carriage-inverted-load_cell-compact.FCStd](./cartesian_frame-Z_carriage-Z_base-X_carriage-inverted-load_cell-compact.FCStd)

[![cartesian_frame-Z_carriage-Z_base-X_carriage-inverted-load_cell-compact.png](./images/cartesian_frame-Z_carriage-Z_base-X_carriage-inverted-load_cell-compact.png)](./cartesian_frame-Z_carriage-Z_base-X_carriage-inverted-load_cell-compact.FCStd)

Assembly prototype:

![assembly2.png](./images/assembly2.png)

### Baseplate

[baseplate-base_parts.FCStd](./baseplate-base_parts.FCStd)

[![baseplate-base_parts.png](./images/baseplate-base_parts.png)](./baseplate-base_parts.FCStd)

Half baseplate for the new short-long frame:

![baseplate-half.png](./images/baseplate-half.png)

[baseplate-assembly3.FCStd](./baseplate-assembly3.FCStd)

[![baseplate-assembly3.png](./images/baseplate-assembly3.png)](./baseplate-assembly3.FCStd)

### Cable management

[cable_carrier-assembly3-bearing_seats.FCStd](./cable_carrier-assembly3-bearing_seats.FCStd)

[![cable_carrier-assembly3-bearing_seats.png](./images/cable_carrier-assembly3-bearing_seats.png)](./cable_carrier-assembly3-bearing_seats.FCStd)

[cable_carrier-assembly3-chain_links.FCStd](./cable_carrier-assembly3-chain_links.FCStd)

[![cable_carrier-assembly3-chain_links.png](./images/cable_carrier-assembly3-chain_links.png)](./cable_carrier-assembly3-chain_links.FCStd)

[cable_carrier-basic_parts.FCStd](./cable_carrier-basic_parts.FCStd)

[![cable_carrier-basic_parts.png](./images/cable_carrier-basic_parts.png)](./cable_carrier-basic_parts.FCStd)

### Aluminium extrusion frames

[cartesian_frame-2020_profiles-assembly3.FCStd](./cartesian_frame-2020_profiles-assembly3.FCStd)

[![cartesian_frame-2020_profiles-assembly3.png](./images/cartesian_frame-2020_profiles-assembly3.png)](./cartesian_frame-2020_profiles-assembly3.FCStd)

[cartesian_frame-2020_profiles-drop_in_nut.FCStd](./cartesian_frame-2020_profiles-drop_in_nut.FCStd)

[![cartesian_frame-2020_profiles-drop_in_nut.png](./images/cartesian_frame-2020_profiles-drop_in_nut.png)](./cartesian_frame-2020_profiles-drop_in_nut.FCStd)

[cartesian_frame-2020_profiles-joining-plate.FCStd](./cartesian_frame-2020_profiles-joining-plate.FCStd)

[![cartesian_frame-2020_profiles-joining-plate.png](./images/cartesian_frame-2020_profiles-joining-plate.png)](./cartesian_frame-2020_profiles-joining-plate.FCStd)

[cartesian_frame-2020_profiles.FCStd](./cartesian_frame-2020_profiles.FCStd)

[![cartesian_frame-2020_profiles.png](./images/cartesian_frame-2020_profiles.png)](./cartesian_frame-2020_profiles.FCStd)

### Electronics

[electronics-box-assembly3.FCStd](./electronics-box-assembly3.FCStd)

[![electronics-box-assembly3.png](./images/electronics-box-assembly3.png)](./electronics-box-assembly3.FCStd)

[electronics-crude_switch_box.FCStd](./electronics-crude_switch_box.FCStd)

[![electronics-crude_switch_box.png](./images/electronics-crude_switch_box.png)](./electronics-crude_switch_box.FCStd)

[electronics-end_stop-rod_fastener-8mm_and_12mm.FCStd](./electronics-end_stop-rod_fastener-8mm_and_12mm.FCStd)

[![electronics-end_stop-rod_fastener-8mm_and_12mm.png](./images/electronics-end_stop-rod_fastener-8mm_and_12mm.png)](./electronics-end_stop-rod_fastener-8mm_and_12mm.FCStd)

### GT2 timing belts

[GT2-20T_idler-smooth.FCStd](./GT2-20T_idler-smooth.FCStd)

[![GT2-20T_idler-smooth.png](./images/GT2-20T_idler-smooth.png)](./GT2-20T_idler-smooth.FCStd)

[GT2-20T_idler.FCStd](./GT2-20T_idler.FCStd)

[![GT2-20T_idler.png](./images/GT2-20T_idler.png)](./GT2-20T_idler.FCStd)

[GT2-belt_lock-thin.FCStd](./GT2-belt_lock-thin.FCStd)

[![GT2-belt_lock-thin.png](./images/GT2-belt_lock-thin.png)](./GT2-belt_lock-thin.FCStd)

[GT2-belt_lock-with_base.FCStd](./GT2-belt_lock-with_base.FCStd)

[![GT2-belt_lock-with_base.png](./images/GT2-belt_lock-with_base.png)](./GT2-belt_lock-with_base.FCStd)

### Steel rods

[steel_rods-smooth.FCStd](./steel_rods-smooth.FCStd)

[![steel_rods-smooth.png](./images/steel_rods-smooth.png)](./steel_rods-smooth.FCStd)

### Pipette adapter

[tools-pipette-gilson-adapter_parts.FCStd](./tools-pipette-gilson-adapter_parts.FCStd)

[![tools-pipette-gilson-adapter_parts.png](./images/tools-pipette-gilson-adapter_parts.png)](./tools-pipette-gilson-adapter_parts.FCStd)



[tools-pipette-gilson-assembly3_adapter.FCStd](./tools-pipette-gilson-assembly3_adapter.FCStd)

[![tools-pipette-gilson-assembly3_adapter.png](./images/tools-pipette-gilson-assembly3_adapter.png)](./tools-pipette-gilson-assembly3_adapter.FCStd)

[tools-pipette-gilson-tip_probe.FCStd](./tools-pipette-gilson-tip_probe.FCStd)

[![tools-pipette-gilson-tip_probe.png](./images/tools-pipette-gilson-tip_probe.png)](./tools-pipette-gilson-tip_probe.FCStd)

[tools-toolchanger-pipette_adapter_extras.FCStd](./tools-toolchanger-pipette_adapter_extras.FCStd)

[![tools-toolchanger-pipette_adapter_extras.png](./images/tools-toolchanger-pipette_adapter_extras.png)](./tools-toolchanger-pipette_adapter_extras.FCStd)

### V1 Tool-changer: mini-latch and magnets

[tools-toolchanger-toolpost-assembly.FCStd](./tools-toolchanger-toolpost-assembly.FCStd)

[![tools-toolchanger-toolpost-assembly.png](./images/tools-toolchanger-toolpost-assembly.png)](./tools-toolchanger-toolpost-assembly.FCStd)

[tools-toolchanger-toolpost-dock.FCStd](./tools-toolchanger-toolpost-dock.FCStd)

[![tools-toolchanger-toolpost-dock.png](./images/tools-toolchanger-toolpost-dock.png)](./tools-toolchanger-toolpost-dock.FCStd)

[tools-toolchanger-toolpost-mini_latch.FCStd](./tools-toolchanger-toolpost-mini_latch.FCStd)

[![tools-toolchanger-toolpost-mini_latch.png](./images/tools-toolchanger-toolpost-mini_latch.png)](./tools-toolchanger-toolpost-mini_latch.FCStd)

### V2 Tool-changer: threaded Jubilee mod

See: [toolchanger-interface-jubilee-kfl08_nut.FCStd](./toolchanger-interface-jubilee-kfl08_nut.FCStd)

[![toolchanger-interface-jubilee-kfl08_nut.png](./images/toolchanger-interface-jubilee-kfl08_nut.png)](./toolchanger-interface-jubilee-kfl08_nut.FCStd)

