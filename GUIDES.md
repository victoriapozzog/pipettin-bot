> This content is not yet in a "readable" state.

Table of contents:

[[_TOC_]]

# Development

> TO-DO

<!--
3) Guia de contributing/developer (todo lo que hay hoy en los readme basicamente)
 - Explicacion de la arquitectura
 - Explicacion de cada modulo (protocol2gcode, gui, etc) y como usarlos y correrlos por separado
 - Como instalar los requires (mongo, python, etc) desde 0 en una raspberry pelada u otro linux
-->

## Overview

A conceptual overview of the main components and their interactions:

- A graphical web interface made in Node.js is served by a Raspberry Pi 4.
    - The web app allows users to build workspaces and protocols, through a friendly UI.
    - The app's backend interacts with a python module through a websocket.
    - The python module translates the protocols to GCODE, and operates the machine.
- The Raspberry Pi runs Raspbian OS, and operates:
  - An Arduino running Klipper through USB.
  - The Klipper "host" software, including Moonraker and Mainsail.
  - Web servers for: The protocol designer GUI, GitBuilding documentation, Jupyter Lab notebooks, etc.
- The Arduino runs the Klipper , and operates:
  - A basic 4-axis CNC machine (which moves the pipette around).
  - Toolchanger mechanics.

Background services are managed by `systemd` user units (enabled by default in the RPi OS image).


![graphic_overview](doc/media/overview.svg)





## Software architecture

1. The GUI generates "workspaces" by using platforms as "templates" and filling the `content` of them regarding user definitions in the UI.
2. Then the user defines a "protocol" for that workspace (a sequence of operations over objects in the workspace). The GUI translates the protocol into "High Level Commands" as representations of each operation or group of operations.
3. The "protocol2gcode" python3 module interprets these commands sequentially, and streams the necessary GCODE to the hardware firmware (modified GRBL 1.1h).

Templates for the definitions (of workspaces, protocols, and platforms) are in JSON format. Those must be imported to your local mongodb server (learn more at the [protocol2gcode](#protocol2gcode) section).

## Software modules

### GUI

ReactJS webapp to create workspaces, contents and protocols.

### protocol-builder

JS library that interprets the user input, and generates protocol.json files.

### workspace-builder

JS library that interprets the user input, and generates workspace.json files.

### piper

Python library that interprets protocol files and generate/stream GCODE to Klipper.

After the GUI defines workspace, platforms and protocol, it saves all elements it in the mongodb database.
Then, it calls this python library to interpret it, and generate/stream the GCODE.

The library runs as a systemd service, and communicates with the GUI through SocketIO.
It can be used without the GUI, as long as the workspace objects are in the Mongo database.

see [klipper/README.md](./code/klipper/README.md) and [defaults/README.md](defaults/README.md) for instructions on setting up testing without the GUI, using the default JSON templates.

## Arduino Firmware

Now using Klipper.

## Hardware

Hardware _at a glance_.

### Electronics

* Two Arduino UNO with CNC Shield 3.0 (clone) and limit switches.
* Raspberry Pi 4 (or a desktop computer running GNU/Linux).
* Pololu stepper drivers.
* 24V power supply.
* Two load cells with amplifiers.

Se details at the dedicated [documentation](doc/electronica/README.md) page.

### Structure and Mechanics

Our designs for parts are still being tested, find them in the "models" directory. See the README.md files therein.

Since the micropipette does not need as much structural stiffness in the XYZ axes as a spindle CNC machine, any CNC or 3D printer structure would work just fine.

However, the structure should withstand the force needed to place a pipette tip.

In other words, you are not bound to use our models, except for the pipette tool adapter (also refered to as the "S axis" elsewhere), attached to the CNC's head. The original OT project has lots of adapters: https://github.com/Opentrons/otone_hardware

# Assembly guide

Assembly, set-up, and usage **documentation** is being written using [GitBuilding](https://gitbuilding.io/), and is accesible online through the following links:

* Published documentation (WIP): https://pipettin-bot.gitlab.io/pipettin-grbl-docs/
* Documentation sources: https://gitlab.com/pipettin-bot/pipettin-grbl-docs

Instructions to build and set-up the robot:

- Assembly: https://pipettin-bot.gitlab.io/pipettin-grbl-docs/guides/03_assembly_guide.html
- Setup and calibration:
    - https://pipettin-bot.gitlab.io/pipettin-grbl-docs/guides/usage/01_setup.html
    - https://pipettin-bot.gitlab.io/pipettin-grbl-docs/guides/usage/02_calibration.html

Assembly guide available [here](https://pipettin-bot.gitlab.io/pipettin-grbl-docs/) (WIP).

<!--
2) Guia de Construccion
 - Guia completa para armar el hardware
-->

# Software installation

Instructions to install and configure the software and firmware components.

Currently a bit scattered:

- [GUIDES.md](./GUIDES.md), "Software installation" section (right here)
- The GitBuilding docs: https://pipettin-bot.gitlab.io/pipettin-grbl-docs/guides/assembly/03_electronics.html

## Raspberry Pi

<!-- Guia Instalacion de imagen raspberry 

Estaria bueno poner algo sobre resizear los .img y filesystems, 
para poder poner la imagen en tarjetas SD más pequeñas.
Por ejemplo: https://github.com/Drewsif/PiShrink
Tutorial muy detallado: https://raspberrypi.stackexchange.com/a/56312
-->

Requirements:

* The system's image, find it [here](https://drive.google.com/drive/folders/1sQWp9x0S_202jgzFJBe-YqoJQlnTlY16?usp=sharing).
* 16-64 GB micro SD card.
* Standard command-line tools in a GNU/Linux OS.

### Method 1: Burn the OS image

- [ ] To-do: re-upload this ISO after cleaning it up.

#### gunzip (old method)

Restore our SD card image to a 64 GB SD card using `dd` and `gunzip` from a terminal.

* Replace `YOUR_SD_CARD_DEVICE` above with the path to your SD card, found under `/dev` (for example `/dev/sdx`, as such, without the final slash).
* To find the path to your SD card: unplug your card, then run `lsblk`, plug in your card, and run `lsblk` again, and compare the outputs. The new device should be your SD card.
* **Warning**: If you get the path wrong, you may cause irreparable damage to your computer's filesystem.

```bash
# Replace "YOUR_SD_CARD_DEVICE" with your SD card's actual device name (you may use "lsblk" to find it).
gunzip --stdout pipettin_pi.img.gz | sudo dd bs=4M status=progress of=/dev/YOUR_SD_CARD_DEVICE
sync
```

Details at: https://www.raspberrypi.org/documentation/linux/filesystem/backup.md

#### pishrink ISO (new method)

We have used `pishrink` on out system image, reducing it to 13 GB. You can use any card with 16 GB or more, yay!

1. Insert the SD card into your computer.
2. Run the command below:
    * Replace `YOUR_SD_CARD_DEVICE` above with the path to your SD card, found under `/dev` (for example `/dev/sdx`, as such, without the final slash).
    * To find the path to your SD card: unplug your card, then run `lsblk`, plug in your card, and run `lsblk` again, and compare the outputs. The new device should be your SD card.
    * **Warning**: If you get the path wrong, you may cause irreparable damage to your computer's filesystem.

```bash
# Replace "YOUR_SD_CARD_DEVICE" with your SD card's actual device name (you may use "lsblk" to find it).
unzip -p pipettin_pi_pishrunk.img.zip | sudo dd bs=4M status=progress of=/dev/YOUR_SD_CARD_DEVICE && sync
```

3. Safely remove the SD card from your computer.
4. Insert it into your Raspberry Pi, and power it on.
5. Find its IP address, use `ssh` to log in, and check if the filesystem has expanded with `df -h`.
    - If not expanded, reboot the Pi. It sometimes takes a couple boots.
    - User and pass are the Pi's default.

### Create OS backup

Note: extra details at [raspberrypi.org archive](https://web.archive.org/web/20210419061127/https://www.raspberrypi.org/documentation/linux/filesystem/backup.md).

To create the compressed filesystem backup, we used:

```bash
# Replace "sda" with your SD card's actual device name (you may use "lsblk" to find it).
sudo dd bs=4M status=progress if=/dev/YOUR_SD_CARD_DEVICEYOUR_SD_CARD_DEVICE | gzip > pipettin_pi.img.gz
sync
```

Uncompressed (very large) images can be saved as well:

```bash
# Replace "sda" with your SD card's actual device name (you may use "lsblk" to find it).
sudo dd bs=4M status=progress if=/dev/YOUR_SD_CARD_DEVICE of=pipettin_pi.img
sync
```

Such an image can be automatically shrung using [PiShrink](https://github.com/Drewsif/PiShrink). The main benefit is that the filesystem is also shrunk, so it can be used in SD cards smaller than the original.

```bash
# Do a compressed backup before shrinking, just in case!
gzip -k pipettin_pi.img

# Shrink the image with PiShrink
sudo pishrink.sh pipettin_pi.img

# Compress the shrunk image, for sharing.
zip pipettin_pi.img.zip pipettin_pi.img
```

### Method 2: From scratch

This method takes a _long_ time, because the compilation of mongodb takes forever.

> These steps cover the broad process, but are not thorough. Effort is required. Good luck!

- [ ] TO-DO: be more thorough.

1. Install Raspi OS on an SD card, put it on the Pi, and power it on.
2. Connect through `ssh` and make a shallow and recursive clone of the repo at the home directory (i.e. `/home/pi`):

```bash
git clone --recurse-submodules -j8 --depth 1 https://gitlab.com/pipettin-bot/pipettin-bot.git
```

3. Install and setup dependencies:
    - GUI (node and mongo): [pipettin-gui](./code/pipettin-gui)
    - Commander (python): [piper](./code/klipper/piper)
    - PC**R** planner (R): [r_protocol_planner](./code/r_protocol_planner)
4. Install and enable the Systemd units: [systemd.units](./code/systemd.units)
5. Load the "default" objects into MongoDB: [defaults](./defaults)

## Arduino firmware

<!-- Guia Instalacion de Klipper en Arduino UNO -->

Standard Klipper procedure: https://www.klipper3d.org/Installation.html#building-and-flashing-the-micro-controller

Make sure to select "Armega328p" as the target microcontroller/MCU.

Because the Arduino UNO clones we used are not excellent quality, they share the device ID. This collision problem can be avoided by pointing Klipper to the device's "path" instead.

# [User guide](https://pipettin-bot.gitlab.io/pipettin-grbl-docs/guides/01_user_guide.html)

- [ ] TO-DO: be more thorough.

<!-- Aca no explicaria nada de la arquitectura ni de los modulos que intervienen). Este deberia ser el readme.md principa. -->

Published with GitBuilding, available at: https://pipettin-bot.gitlab.io/pipettin-grbl-docs/guides/01_user_guide.html

Sources: https://gitlab.com/pipettin-bot/pipettin-grbl-docs

Instructions to set-up new platforms and tools, define protocols, and running them.

- Usage: https://pipettin-bot.gitlab.io/pipettin-grbl-docs/guides/01_user_guide.html
- Platform and tool set-up: 

## Web GUI guide

<!-- Manual de GUI -->

- [ ] TO-DO: be more thorough.

## Machine calibration

Callibration instructions before protocol run.

### Tool calibration

A procedure is available at: [`doc/tool_callibration/README.md`](./doc/tool_callibration/README.md)

<!-- Calibracion del XYZ de las herramientas y sus offsets. -->

- [ ] TO-DO: be more thorough.

### Workspace and platforms

A procedure is available at: [`doc/platforms/README.md`](./doc/platforms/README.md)

<!-- Calibracion del XYZ de los objetos en la mesa. -->

- [ ] TO-DO: be more thorough.

### Editing platforms for multi-tool support

A procedure is available at: [`doc/tool_callibration/README.md`](./doc/tool_callibration/README.md)

<!-- Calibracion del Z de los objetos en la mesa para cada tool. -->

- [ ] TO-DO: be more thorough.

### Pipette volumetric calibration

Calibration results are available at the [`calibration`](./doc/calibration) directory (see its [README.md](./doc/calibration/README.md)).

<!-- 
Calibracion de pipeta:

- Relacion Volumen-deplazamiento.
- Tip probe.
- Setup de las constantes en el driver (retraction, etc.).
- Setup de las correcciones en el driver (pipeteo de mas / de menos, etc.).
- Protocolo de calibracion con balanza analítica.
-->

- [ ] TO-DO: be more thorough.
