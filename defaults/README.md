# MongoDB default documents

- **Platforms**: representations of physical lab objects.
- **Workspaces**: arrangements of platforms in a virtual space.
- **Protocols**: sequences of pipetting steps, associated to a certain workspace.

# Requisites

MongoDB must be running. See [README.md](../gui/README.md) for details on the setup.

# Examples

The _update_ example is probably all you will ever need.

## Update to latest

Drop tables from mongo:

```
mongo
> use pipettin
> db.platforms.drop()
> db.protocols.drop()
> db.workspaces.drop()
```

Get the latest objects from the remote git repo and import them:

```bash
cd ~/pipettin-grbl/defaults/
git pull
mongoimport --db pipettin --collection platforms --file latest/platforms_export.json --jsonArray
mongoimport --db pipettin --collection protocols --file latest/protocols_export.json --jsonArray
mongoimport --db pipettin --collection workspaces --file latest/workspaces_export.json --jsonArray
```

## Export

General syntax:

```
mongoexport -d <database> -c <collection_name> -o <output_file.json> --jsonArray
```

Examples:

```bash
mongoexport -d pipettin -c platforms  -o platforms_export.json --jsonArray
mongoexport -d pipettin -c workspaces -o workspaces_export.json --jsonArray
mongoexport -d pipettin -c protocols  -o protocols_export.json --jsonArray
```

### Pretty JSON

Prettyfy the output using BASH and Python (see https://stackoverflow.com/a/1920585):

```bash
cat platforms_export.json | python -m json.tool > platforms_export.pretty.json
cat workspaces_export.json | python -m json.tool > workspaces_export.pretty.json
cat protocols_export.json | python -m json.tool > protocols_export.pretty.json
```

Delete the old ones optionally:

```bash
rm platforms_export.json workspaces_export.json protocols_export.json
```

## Import

Latest default platforms are in the [latest](./latest) directory.

A document holding a single platform can be imported with `mongoimport`:

```bash
mongoimport --db pipettin --collection platforms --file platform.json
```

Documents holding multiple platforms each, can be imported with `mongoimport --jsonArray`:

> Note: the document _must_ be a JSON array. The command will fail if the JSON document is not an array ([ref](https://stackoverflow.com/a/40524744).). Each definition must be enclosed in curly braces ( `{}`), separated with commas (`,`) and the whole file must be enclosed with square brackets ( in `[]`).

```bash
mongoimport --db pipettin --collection platforms --file platforms.json --jsonArray
mongoimport --db pipettin --collection platforms --file platforms_enanas.json --jsonArray
mongoimport --db pipettin --collection workspaces --file workspaces.json --jsonArray
mongoimport --db pipettin --collection protocols --file protocols.json --jsonArray
mongoimport --db pipettin --collection protocols --file protocols_for_tests.json --jsonArray
mongoimport --db pipettin --collection protocols --file protocol_test1.json --jsonArray

# Or, for short: https://stackoverflow.com/a/44195823
for filename in proto*s.json; do mongoimport --db pipettin --collection protocols --jsonArray $filename; done;
for filename in plat*s.json; do mongoimport --db pipettin --collection platforms --jsonArray $filename; done;
for filename in work*s.json; do mongoimport --db pipettin --collection workspaces --jsonArray $filename; done;
```

Notes:

* In the Raspberry Pi 4 I had to append `--jsonArray` to these commands to fix the  following error: `BSON representation of supplied JSON is too large`.

The mongo shell can be used to check the imported contents:

```
$ mongo
> use pipettin
> db.workspaces.find().pretty()
> db.protocols.find().pretty()
```

# Troubleshooting

You may need to set "export LC_ALL=C" in bash if you get the following error:

    BadValue Invalid or no user locale set. Please ensure LANG and/or LC_* environment variables are set correctly

See: https://stackoverflow.com/questions/26337557/
