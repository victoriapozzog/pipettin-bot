# Contribute to this project

> We try to make it as welcoming and _free_ as we can.

The project's development lives on GitLab; a website that hosts a version control system for files (called git). In GitLab you can keep track of tasks and objectives by opening _issues_. We use this system to organize the development.

Get in touch:

- Discord server: https://discord.gg/nQxZaaja
- GitLab: https://gitlab.com/pipettin-bot/pipettin-grbl/-/boards/4361386

Where to find things to do:

Development:
- There are GitLab issues at [the main board](https://gitlab.com/pipettin-bot/pipettin-grbl/-/boards/4361386) on different aspects of the project, which need attention and discussion.
- If you want to open a discussion about somethin interesting to you, please post a new issue at the [contrtibutions and support board](https://gitlab.com/pipettin-bot/pipettin-grbl/-/boards/4685246).
- Pull-requests are also very welcome! be it in README contents, code, an 3D models.

Documentation:
- Online documentation can be [annotated directly](https://pipettin-bot.gitlab.io/pipettin-grbl-docs#comment-on-the-documentation), using Hypothes.is (it's free, as in beer and freedom).
- Documentation issues live in an independent repo, here: https://gitlab.com/pipettin-bot/pipettin-grbl-docs/-/boards
- Comment issues, open issues, feel at home!

You'll find the project's _issues_, taged with colored _labels_, and arranged in an _issue board_ like this one:

[![issue_boards.svg](./doc/media/images/issue_boards.svg)](https://gitlab.com/pipettin-bot/pipettin-grbl/-/boards/4361386)

# Roadmap

- [x] Version 1: GRBL firmware, Express.js GUI, Gilson micropipettes.
- [ ] Version 2 _WORK IN PROGRESS _: Klipper firmware, ReactJS GUI, PyLabRobot API, custom micropipettes.
- [ ] Version 3: Interoperability with OT2 labware and protocols.

## Open Lab automation landscape

Have a look at out "list" and "interaction" issue labels:

- Interactions: https://gitlab.com/pipettin-bot/pipettin-grbl/-/issues?label_name%5B%5D=interactions
- Lists: https://gitlab.com/pipettin-bot/pipettin-grbl/-/issues?label_name%5B%5D=lists
- Map conceptualization issue: https://gitlab.com/pipettin-bot/pipettin-grbl/-/issues/96

![automation_overview.svg](./doc/meta/automation_overview.svg)
